﻿
<!-- Hier die Links anpassen '../../php/system/data.php' und '../../php/system/security.php' -->
<?php include_once('../../php/system/data.php')?>
<?php include_once('../../php/system/security.php')?>

<!-- Hier ist der Platz für eigene PHP Befehle -->
<?php include_once('insert.php')?>

<?php
    //Liebe Grüsse vom Backend-Team

    /*
    session_start(); //start/continue the session

     //create a new one-time-user:

    if (is_numeric($_GET['userIdentifier'])) { //make sure userIdentifier is a number

    $user_identifier = $_GET['userIdentifier']; //get userIdentifier from url via GET
}

   $userID = new_oneTimeUser($user_identifier); //create a new one-time-user in db and return id -> our userID

if(is_numeric($_GET['wsID'])) { //make sure wsID is a number
    $wsID = $_GET['wsID']; //get wsID from url via GET
}

    $ws_user_result = new_workshopforuser($wsID, $userID); //create connection between new one-time-user and workshop

   //depending on what you need:

   $_SESSION['userID'] = $userID; //save userID in a session-variable
   $_SESSION['wsID'] = $wsID; //save wsID in a session-variable
       */

    ?>




<!doctype html>
<html lang="de">

	<head>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

		<!-- JQuery -->
		<script
  		src="https://code.jquery.com/jquery-3.3.1.min.js"
  		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  		crossorigin="anonymous"></script>

		<!-- Ajax Popper -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

		<!-- Bootstrap Javascript -->
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

		<!-- Font Awesome CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<!-- Eigenes CSS -->
		<link rel="stylesheet" type="text/css" href="stylesheet.css" media="screen" />

		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    	<title>Bewertung</title>

	</head>

	<body>

		<header>

 			    <!-- Lädt Gamification-Button und Timer-Zeile:
           An dieser Stelle wird der Code der Usability-Bar (Timer und Co.) eingebettet.
           Über die id lässt sich bestimmen, welche Button eingeblendet werden.-->
  		 	<div class="timer-buttons" id="bar-b"></div>
       	 	<!-- Lädt Einfuehrung in Uebung
               Sobald die Seite geladen ist, wird das Modal mit der Einführung eingeblendet.-->
        	<div class="intro-b"></div>

		</header>

	  	<nav></nav>

	  	<!--ID anpassen auf Projektnamen z.B. Brain2Go -->
	  	<main id="bewertung">
			<div class="container-fluid">
				<div class="row">
					<!-- Linke Spalte -->
  					<div class="col-md-6">
						<div class="container">
							<h2 class="font-bg text-center">Bewerte nun die Ideen</h2><br>
							<h4 class="font-md text-center">Bitte auf Massnahme klicken, um Bewertung abzugeben.</h4><br>
							<hr /><br>
							<!-- Accordion wird automatisch mit Content aus der Datenbank gefüllt -->
							<div id="accordion">

							<!-- Aufrufen der Datenbank in einer Schlaufe -->
							<?php

							require_once ('index.php');
							$db_link = mysqli_connect ('localhost', '618277_12_1', 'S=@Hl3KhEGpA', '618277_12_1');
							$sql = "SELECT * FROM `idea`";
							$db_erg = mysqli_query( $db_link, $sql);
							while($build = mysqli_fetch_array($db_erg))

							{

								?>

								<!-- Aufrufen einer Card mittels der ideaID aus der Datenbank -->
								<div class="row" id="card-id-<?php echo $build['ideaID'] ?>">
  									<div class="card col-md-10">
    									<div class="card-header" id="headingOne" align="center">
      										<h5 class="mb-0">
												<!-- Button welcher als Data-Target die id des Card Body besitzt -->
        										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $build['ideaID'] ?>" aria-expanded="true" aria-controls="collapseOne">
													<!-- Aufrufen des ideaTitle aus der Datenbank für die Beschriftung des Card Header -->
													<?php echo $build['ideaTitle'] ?>
        										</button>
      										</h5>
    									</div>
										<!-- Aufrufen des Card Body mittels der Ergänzung der id mit ideaID aus der Datenbank -->
    									<div id="collapse-<?php echo $build['ideaID'] ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
     										<div class="card-body" align="center">
												<!-- Aufrufen des ideaDescription aus der Datenbank für die Beschreibung im Card Body -->
												<?php echo $build['ideaDescription'] ?>
		  										<div class="bewertung">
		  											<br><br><br>
													<h4 class="font-md text-center" alt="Simple">Bitte Bewerten</h4>
													<!-- Verlinkung der einzelnen Sterne (Bewertung 1-10) mit der ideaID, damit diese bei jeder Card neu geladen werden und damit später die Werte an den richtigen Ort in der Datenbank übergeben werden können -->
													<div class="container">
        												<div class="starrating risingstar d-flex justify-content-center flex-row-reverse">
															<input type="radio" id="star10-<?php echo $build['ideaID'] ?>" name="rating" value="10" starid="<?php echo $build['ideaID'] ?>"/><label for="star10-<?php echo $build['ideaID'] ?>" title="10 star" >10</label>
															<input type="radio" id="star9-<?php echo $build['ideaID'] ?>" name="rating" value="9" starid="<?php echo $build['ideaID'] ?>"/><label for="star9-<?php echo $build['ideaID'] ?>" title="9 star">9</label>
															<input type="radio" id="star8-<?php echo $build['ideaID'] ?>" name="rating" value="8" starid="<?php echo $build['ideaID'] ?>"/><label for="star8-<?php echo $build['ideaID'] ?>" title="8 star">8</label>
															<input type="radio" id="star7-<?php echo $build['ideaID'] ?>" name="rating" value="7" starid="<?php echo $build['ideaID'] ?>"/><label for="star7-<?php echo $build['ideaID'] ?>" title="7 star">7</label>
															<input type="radio" id="star6-<?php echo $build['ideaID'] ?>" name="rating" value="6" starid="<?php echo $build['ideaID'] ?>" /><label for="star6-<?php echo $build['ideaID'] ?>" title="6 star">6</label>
           													<input type="radio" id="star5-<?php echo $build['ideaID'] ?>" name="rating" value="5" starid="<?php echo $build['ideaID'] ?>" /><label for="star5-<?php echo $build['ideaID'] ?>" title="5 star">5</label>
            												<input type="radio" id="star4-<?php echo $build['ideaID'] ?>" name="rating" value="4" starid="<?php echo $build['ideaID'] ?>"/><label for="star4-<?php echo $build['ideaID'] ?>" title="4 star">4</label>
            												<input type="radio" id="star3-<?php echo $build['ideaID'] ?>" name="rating" value="3" starid="<?php echo $build['ideaID'] ?>"/><label for="star3-<?php echo $build['ideaID'] ?>" title="3 star">3</label>
            												<input type="radio" id="star2-<?php echo $build['ideaID'] ?>" name="rating" value="2" starid="<?php echo $build['ideaID'] ?>"/><label for="star2-<?php echo $build['ideaID'] ?>" title="2 star">2</label>
            												<input type="radio" id="star1-<?php echo $build['ideaID'] ?>" name="rating" value="1" starid="<?php echo $build['ideaID'] ?>"/><label for="star1-<?php echo $build['ideaID'] ?>" title="1 star">1</label>
        												</div>
  													</div>
		  										</div>
      										</div>
    									</div>
  									</div>
									<!-- Roter Button um die bereits bewerteten Ideen wieder zurück zu setzen. Dieser ist mit der ideaID verknüpft damit er auch eindeutig einer Card im Accordion zugewiesen werden kann. -->
									<div class="col-md-2">
										<i class="fas fa-minus-circle fa-3x" style="color: tomato" starid="<?php echo $build['ideaID'] ?>"></i>
									</div>
								</div>

							<!-- Ende der Schlaufe -->
							<?php

							}

								?>

							</div>
						</div>
					</div>
					<!-- Rechte Spalte -->
					<div class="col-md-6">
						<div class="container">
							<h2 class="font-bg text-center">Meine Bewertungen</h2><br>
							<h4 class="font-md text-center">Bitte auf Massnahme klicken, um Bewertung zu ändern.</h4><br>
							<hr /><br>
							<!-- Leerer Container für die Übertragung einzelner Cards mittels appendTo -->
							<div id="right"></div>
    					</div>
  					</div>
  				</div>
			</div>




		<script>

			$(".fa-minus-circle").click(function(e) {
				$(this).parent().parent().appendTo("#accordion");
				var rating = $(this).val();
				var starid = $(this).attr("starid");
				var oldRating = (localStorage.getItem("rating-" + starid) == null ? 0 : localStorage.getItem("rating-" + starid));


				// if abfrage mit localstorage.getitem(....), die starid abfragen. (wenn false/"" dann weiter)

				$.ajax({
					url:"insert.php",
					method:"POST",
						data:{
							rating: (rating - oldRating),
							punktzahlmax: -10,
							starid: starid,
					},
					success: function(data){
						console.log(data);
						$('#result').html(data);
						// Setze local storage mit starid = true
						//localStorage.setItem(starid=true);
					},
					fail: function (data) {
						console.log(data);
					}
				});
			});

			$('input[type="radio"]').click(function(e){
				var rating = $(this).val();
				var starid = $(this).attr("starid");
				var oldRating = (localStorage.getItem("rating-" + starid) == null ? 0 : localStorage.getItem("rating-" + starid));
				oldRating = 0;

				if(rating > 0){

				//anfrage_abschicken();

					alert("Erfolg");

					$("#card-id-" + starid).appendTo("#right");


				}else{
					alert("fail");
				}


			// if abfrage mit localstorage.getitem(....), die starid abfragen. (wenn false/"" dann weiter)

				$.ajax({
					url:"insert.php",
					method:"POST",
						data:{
							rating: (rating - oldRating),
							punktzahlmax: (oldRating == 0 ? 10 : 0),
							starid: starid,
					},
					success: function(data){
						console.log(data);
						$('#result').html(data);

					},
					fail: function (data) {
						console.log(data);
					}
				});

				localStorage.setItem("rating-" + starid, rating);

			});

		</script>

		</main>

	  	<footer></footer>

    	<!-- Dokumente für Gamification:
           Damit werden die php-Dateien vom Usability-Team ins Dokument geladen.
           (Achievments-PHP hat momentan noch keine Funktionen drin,
           da zuerst alle Übungen fertiggestellt werden müssten.)-->
      	<?php include '../../php/gamification/gamification.php';?>
      	<?php include '../../php/gamification/achievements.php';?>

	</body>
</html>
