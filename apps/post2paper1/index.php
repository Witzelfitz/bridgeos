<?php session_start(); ?>

<!-- Hier die Links anpassen '../../php/system/data.php' und '../../php/system/security.php' -->
<?php include_once('../../php/system/data.php')?>
<?php include_once('../../php/system/security.php')?>

<!-- Hier ist der Platz für eigene PHP Befehle -->



<!doctype html>
<html lang="de">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <!-- BridgeOS CSS -->
    <link rel="stylesheet" href="http://618277-12.web1.fh-htwchur.ch/bridgeos/css/import.css" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/style.css" />
    <link rel="stylesheet" href="../../css/style_post2paper1.css" />
	<!--Titel anpassen auf Projektnamen z.B. Brain2Go -->
    <title>Post2Paper1</title>

  </head>


    
<body>


  <!-- Lädt Gamification-Button und Timer-Zeile-->
<div class="timer-buttons" id="bar-p2p1"></div>
    <!-- Lädt Einfuehrung in Uebung-->
    <div class="intro-p2p1"></div>


<header></header>
<nav></nav>
<!--ID anpassen auf Projektnamen z.B. Brain2Go -->
<main id="Post2Paper1"></main>
<div class="bg">


<!-- Post2Paper1 body-->
	<div class="container">
		<div class="game">
			<div class="post2paper1">
				<div class="col-lg-12">


<!-- Bild -->

    <div class="col-lg-4"  style="margin-top: 20px;">
      <div class="post2paper1-postit-left">
        <div class="post2paper1__image" >
            
<!--Bild-Abfrage aus Datenbank, zuerst wird Datenbankverbindung hergestellt, dann die Funktion get_image aufgerufen und mit mysqli_fetch_assoc in einem Array gespeichert, der mit Echo ausgegeben wird. -->
            
            <?php
            $link = get_db_connection();
            $result = get_image(9);
                if (! $result)
                {die ('Ungültige Abfrage:'.mysqli_error());
                }
            while ($row = mysqli_fetch_assoc($result)) { ?>
                <img src="<?php echo $row["imgURL"] ?>">
                <!--echo '<img src="$row["imgURL"]"/>';-->
            <?php } ?>
            
            </div>
        </div>
    </div>


<!-- Frage -->
<!-- Hier wird mittels questionID die vom Moderator gestellte Frage aus der Datenbank importiert und mit Echo ausgegeben -->

    <div class="col-lg-8">
        <div class="post2paper1__question" style="margin-top: 20px;" >
            
            <?php
            $questionID = 2;
            echo get_question_text($questionID);
            ?>
            
        </div>
    </div>


<!-- Antworten -->

    <div class="container">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="comment">Hier Antwort eingeben:</label>

<!-- Formular -->
<!-- Das Eingabefeld, Anzahl Buchstaben beschränkt auf 30, das eingegebene Wort wird nach dem Absenden durch Klicken auf das Eingabefeld durch onfocus gelöscht-->

    <div class="col-lg-6">
    <form id="post2paperform" method="post">
    <input type="text" name="post2paper1" id="post2paper1" onfocus="this.value=''"  maxlength="30"/>
    <input type="submit" name="submit" class="btn btn-primary" value="Antwort senden"/>


<!-- Antwort wird am Formular übermittelt, sobald auf den Submit-Button oder Enter gedrückt wird. Das eingegebene Wort wird in der Variable $answer gespeichert und, sofern das Feld nicht leer ist, mit der Funktion new_answer als neue Row in die answer-Tabelle der Datenbank eingefügt   -->

    <?php

        if(isset($_POST['submit'])){
            $answer = strip_tags($_POST['post2paper1']);
            if(!empty($_POST['post2paper1'])){
                new_answer($answer, 0, 0, 1, questionid, userid);

                echo "error=". mysqli_error($link);
    }}?>

    </form>
    </div>




<!-- Antworten auf Seite -->
<!-- In einer ungeordneten Liste wird die Eingabe ins Antworten-Feld bei submit direkt auf der Seite aufgelistet, neue Eingaben werden mittels for-Schleife hinzugefügt, bis die Session neu geladen wird -->

  <div id="result">
    <div class="col-lg-6">
        <div class="post2paper1__answer-list"> 
        <ul id="list"> 
        <div class="post2paper1-answerlist-bg"></div>
        </ul>


        <script type="text/javascript">
            var allresults = [];
            document.querySelector("#post2paperform").addEventListener("submit", function(e){

                allresults.push(document.getElementById("post2paper1").value);
                console.log(allresults);

                var ul = document.getElementById("list");

                while( ul.firstChild ){
                    ul.removeChild( ul.firstChild );
                }

                for (var i = 0; i < allresults.length; i++) {
                    var li = document.createElement("li");
                    li.appendChild(document.createTextNode(allresults[i]));
                    ul.appendChild(li);
                }
                document.getElementById("post2paper1").value = "";
                e.preventDefault();
            })
        </script>

        </div></div>      
        </div></div>
        </div></div>
        </div></div>
        </div>
</div>


<footer></footer>





    <!-- BridgeOS JavaScript -->
    <script src="http://618277-12.web1.fh-htwchur.ch/bridgeos/js/main.js" crossorigin="anonymous"></script>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Dokumente für Gamification: -->
      <?php include '../../php/gamification/gamification.php';?>
      <?php include '../../php/gamification/achievements.php';?>

  </body>



</html>
