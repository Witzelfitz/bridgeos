<?php
session_start();

include_once('../../php/system/data.php');
include_once('../../php/system/security.php');

include "prototyping_uploadFunction.php";

?>

<!-- Hier ist der Platz für eigene PHP Befehle -->


<?php

    // define image array
    $images = [];

    // set session if unset
    if(!isset($_SESSION)){
        session_start();
    }

    // define Session variable if not set (required so that array does not get overriden on submit)
    if(!isset($_SESSION['prototyping_images'])){
        $_SESSION['prototyping_images'] = $images;
    }

?>

<html>

    <head>
        <link rel="stylesheet" href="prototyping.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!-- import.css einbinden -->
         <link rel="stylesheet" href="../../css/import.css" crossorigin="anonymous">

        <!-- prototyping.css einbinden -->
         <link rel="stylesheet" href="../../css/style_prototyping.css" crossorigin="anonymous">


        <!-- BridgeOS CSS -->
        <link rel="stylesheet" href="http://618277-12.web1.fh-htwchur.ch/bridgeos/css/import.css" crossorigin="anonymous">

    	<!--Titel anpassen auf Projektnamen z.B. Brain2Go -->
        <title>Simplyprototyping</title>


    </head>

    <body>


        <!-- Lädt Gamification-Button und Timer-Zeile-->
        <div class="timer-buttons" id="bar-sp"></div>
        <!-- Lädt Einfuehrung in Uebung-->
        <div class="intro-sp"></div>


        <!-- BridgeOS JavaScript -->
        <script src="http://618277-12.web1.fh-htwchur.ch/bridgeos/js/main.js" crossorigin="anonymous"></script>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



        <!-- app -->
        <div class=" ">


            <h3 class=" "> Bitte wähle ein Bild </h3>

            <div>

                <!-- image submit form -->
                <form method="post" enctype="multipart/form-data">
                    
                    <!-- alterative file select method -->

                    <!-- <input type="button" id="loadFileXml" value="Bild auswählen" onclick="document.getElementById('simplyprototyping_file').click();" />
                    <input type="file" style="display:none;" accept="image/*" id="simplyprototyping_file" name="imageSelectField"/> -->

                    <!-- conventional file select button. not so nice -->
                    <input class=" " value="Bild auswählen" type="file" accept="image/*" id="imageSelectField" name="imageSelectField">
                    <input class=" " type="submit" value="Antwort senden" name="submit">
                </form>

                <!-- testing Code for Session handling -->
                <!-- <form method="post" enctype="multipart/form-data">
                    <input type="submit" value="Sessiondestroy" name="unsetSession">  
                </form> -->

                <!-- <h4> testing purpouse </h4>
                <form method="post" enctype="multipart/form-data">
                    <input type="submit" value="Sessiondestroy" name="unsetSession">
                </form> -->

                <!-- testing Code for database -->
                <!-- <form method="post" enctype="multipart/form-data">
                    <input type="submit" value="testimgupload" name="testimgupload">
                </form> -->
            </div>

            <p class=" ">

                <!-- return Uploadmessage and set class required -- error or ok -->
                <?php if(isset($_POST["submit"])) { ?>

                    <?php if($prototyping_uploadOk != 0) { ?>

                        <p class="prototyping uploadMessage ok"> <?php echo $prototyping_uploadMessage; ?> </p>

                    <?php }else{ ?>

                        <p class="prototyping uploadMessage error"> <?php echo $prototyping_uploadMessage; ?> </p>

                    <?php } ?>
                <?php } ?>


                <!-- testing Code -->
                <?php
                    // testing Code for Session
                    // if(isset($_POST["unsetSession"])) {

                    //     session_destroy();
                    //     header("Refresh:0");

                    //     echo "session destroyed";

                    // }

                    // testing Code for database
                    // if(isset($_POST["testimgupload"])) {

                    //     new_image('testpath', 'test', 2);

                    // }
                ?>

            </p>


        </div>

        <div class=" ">

            <!-- testing code --><!-- 
            <?php print_r($prototyping_testvariable) ?> -->


            <!-- print each image saved in Session array -->
            <?php if(isset($_POST["submit"])) { ?>
                
                <!-- only print title of answers if answer is given -->
                <?php if(!empty($_SESSION['prototyping_images'])) { ?>

                    <h3>gesammelte Antworten</h3>

                <?php } ?>


                <!-- print for testing purpouse -->
                <!-- <?php print_r($prototyping_testvariable); ?> -->
                <!-- <?php print_r($_SESSION['prototyping_images']); ?> -->


                <?php foreach($_SESSION['prototyping_images'] as $image_src){ ?>

                    <img class="prototyping answerImg" src= <?php echo $image_src ?> >

                <?php } ?>

            <?php } ?>

        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <!-- Dokumente für Gamification: -->
        <?php include '../../php/gamification/gamification.php';?>
        <?php include '../../php/gamification/achievements.php';?>

    </body>

</html>
