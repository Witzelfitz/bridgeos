<?php
    $prototyping_root_folder = "apps/simplyprototyping/";
    $prototyping_target_folder = "images/";
    $prototyping_uploadOk = 1;
    $prototyping_uploadMessage = "";

    $prototyping_testvariable;

   
    //upload Image code - executed on sumbit
    if(isset($_POST["submit"])) {

        // check if file was selected
        if (!empty($_FILES["imageSelectField"]["tmp_name"])) {        

            //create unique ID for each image -- encode filename to match server requirements -- create file path
            $simplyprototypingID = uniqid();
            $simplyprototyping_encodedFilename = urlencode(basename($_FILES["imageSelectField"]["name"]));

            $prototyping_target_path = $prototyping_target_folder.$simplyprototypingID.$simplyprototyping_encodedFilename;


            //check if file is an image
            $check = getimagesize($_FILES["imageSelectField"]["tmp_name"]);

            if($check !== false) {
                $prototyping_uploadOk = 1;
            } else {
                $prototyping_uploadOk = 0;
                $prototyping_uploadMessage = "File is not an image";
            }


            // // Check if file already exists ---> depricated
            // if (file_exists($prototyping_target_path)) {
            //     $prototyping_uploadOk = 0;
            //     $prototyping_errormessage = "you managed to press UPLOAD in the exact same milisecond as another user. Congrats";
            // }


            // Check file size
            if ($_FILES["imageSelectField"]["size"] > 500000000) {
                $prototyping_uploadOk = 0;
                $prototyping_uploadMessage = "Sorry, your file is too large";
            }


            //upload image if there is no error
            if ($prototyping_uploadOk != 0) {

                // try to upload file
                if (move_uploaded_file($_FILES["imageSelectField"]["tmp_name"], $prototyping_target_path)) {

                    //write into session, necessary to display images
                    $_SESSION['prototyping_images'][] = $prototyping_target_path;

                    //write into db
                    new_image($prototyping_root_folder.$prototyping_target_path, $simplyprototyping_encodedFilename, 2);

                    //write uploadMessage
                    $prototyping_uploadMessage = "The file ". basename( $_FILES["imageSelectField"]["name"]). " has been uploaded.";

                } else {
                    $prototyping_uploadMessage = "Sorry, there was an error uploading your file";
                }
            }

        }else{

            $prototyping_uploadOk = 0;
            $prototyping_uploadMessage = "please select a file";

        }
    }

?>


