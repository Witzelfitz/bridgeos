/************************* Variablen deklarieren *******************************/
var terms = [];
var maxTerms = 14;
var id = 0;
var lastChanges = [];
var save = [];

$(document).ready(function(){
	
	/************************* Die Begriffe zufällig im Array positionieren *******************************/
	terms.sort(function() { return 0.5 - Math.random() });
	
	/************************* Abfragen ob die Anzahl Wörter die Maximale Begriffsanzahl übersteigt *******************************/
	if(terms.length < maxTerms){
		maxTerms = terms.length;
	}
	
	/************************* Felder für Texte erstellen *******************************/
	for (var i = 0; i < maxTerms; i++) {
		randomNum = Math.round(Math.random());
		if(randomNum == 1 || randomNum == 0){
			$('.term-Container').append('<input class="term font-md textboxes term_'+i+'" id="term_'+i+'" data-index="'+id+'" data-id="'+id+'" readonly></input>');	
		}
		id++;
	}
	
	/************************* terms aus Array eintragen *******************************/
	for (var index = 0; index < maxTerms; index++){
		$('.term_'+index).val(terms[index]);
	}
    
	/************************* terms in den Feldern wechseln *******************************/
	var i = 0;
	setInterval(function () {
		defineTerm();
		defineField();

		/************************* Überprüfen ob der Wert in der Savebox gespeichert ist ***************************/
		if(save.length <1){
			fade(i);
		}else{
			for (var e = 0; e < save.length; e++){
				if(index == save[e]){
					defineTerm();
					e = 0;
				}	
			}
			fade(i);
		}		
	},1000);
	
	/************************ definieren welcher Begriff erscheint ******************************/
	function defineTerm(){
		index++;
		if(index >= terms.length){
			index = 0;
		}
	}
	
	function defineField(){
		i = Math.round(Math.random()*maxTerms);
		checkField();
	}
	
	function checkField(){
		for(var e = lastChanges.length - 1; e >= 0; e--) {
			if(lastChanges[e] == i) {
			   defineField();
			   console.log('fail');
			   return false;
			}
		}
		return true;
	}
	
	/****************************** Box aus und mit neuem Begriff einblenden **************************************/
	function fade(i){
		$('.term_'+i).fadeTo(300,0, function() {
			$('.term_'+i).val(terms[index]);
			$('.term_'+i).attr('data-id',index);
		});
		$('.term_'+i).fadeTo(300,1);
		if(lastChanges.length >= 5){
			lastChanges.shift();
			lastChanges.push(i);
		} else{
			lastChanges.push(i);
		}
		console.log(lastChanges);
	}
	
	/****************************** Begriffe in Safebox hinzufügen **************************************/
	$(document).on("click",".term",function() {
		var id = $(this).attr('data-id');
        $('.save-container').append('<div class="saveContainer save_'+id+'"><input class="save" value="'+$(this).val()+'" readonly></input><div class="cross" data-id="'+id+'"></div></div>');
		save.push(id);
		var i = $(this).attr('data-index');
		defineTerm();
		if(save.length <1){
			fade(i);
		}else{
			for (var e = 0; e < save.length; e++){
				if(index == save[e]){
					defineTerm();
					e = 0;
				}	
			}
			fade(i);
		}	
		fade(i);
		console.log(i);
        console.log("clicked on word");
    });
	
	/****************************** Begriffe aus Safebox löschen **************************************/
	$(document).on("click",".cross",function() {
		id = $(this).attr("data-id");
		$('.save_'+id).hide();
		for(var i = save.length - 1; i >= 0; i--) {
			if(save[i] === id) {
			   save.splice(i, 1);
			}
		}
		console.log(save);
    });
});