<?php
//Liebe Grüsse vom Backend-Team

    session_start(); //start/continue the session

     //create a new one-time-user:
    /*
    if (is_numeric($_GET['userIdentifier'])) { //make sure userIdentifier is a number

    $user_identifier = $_GET['userIdentifier']; //get userIdentifier from url via GET
}

   $userID = new_oneTimeUser($user_identifier); //create a new one-time-user in db and return id -> our userID

if(is_numeric($_GET['wsID'])) { //make sure wsID is a number
    $wsID = $_GET['wsID']; //get wsID from url via GET
}

    $ws_user_result = new_workshopforuser($wsID, $userID); //create connection between new one-time-user and workshop

   //depending on what you need:

   $_SESSION['userID'] = $userID; //save userID in a session-variable
   $_SESSION['wsID'] = $wsID; //save wsID in a session-variable

*/
    ?>


  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>Kompression</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="../../css/bootstrap.min.css">

        <link rel="stylesheet" href="../../css/import.css">

		<link href="../../css/style_kompression.css" rel="stylesheet">


		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<!--<script type="text/javascript" src="script_random_position.js"></script>-->
		<script type="text/javascript" src="script.js"></script>
		<?php
			//include('../../php/header.php');
			include('functions.php');
			include('security.php');

			$answers_raw = getAnswers();
			while ($answers = mysqli_fetch_array($answers_raw)) {
				$answerAct = str_replace("'", "\'", $answers['answer']);
				?>
				<script>
					terms.push('<?php echo $answerAct ?>');
					console.log(terms);
				</script>
			<?php }

			if(isset($_POST['submit'])){

				if(!empty($_POST['ideaTitle']) && !empty($_POST['ideaDescription'])){
					$ideaTitle = sql_injection_filter($_POST['ideaTitle']);
					$ideaDescription = sql_injection_filter($_POST['ideaDescription']);
					
					writeIdea($ideaTitle,$ideaDescription);
					echo '<script>alert("Besten Dank für Ihre Teilnahme. Sie werden benachrichtigt, sobald die nächste Phase beginnt.");</script>';
					echo '<script>window.location = "../../";</script>';
				} else{
					echo '<script>alert("Bitte füllen Sie beide Felder aus.");</script>';
				}
			}
		?>
	</head>
	<body>

	<!-- Lädt Gamification-Button und Timer-Zeile-->
    <div class="timer-buttons" id="bar-k"></div>
        <!-- Lädt Einfuehrung in Uebung-->
        <div class="intro-k"></div>


		<form method="post" class="container">

		<div class="row">
            <input class="idea col-lg-3 input-group-text font-bg" type="text" name="ideaTitle" placeholder="IDEE">
            <textarea class="description col-lg-7 input-group-text font-bg" name="ideaDescription" placeholder="BESCHREIBUNG"></textarea>
			<button type="submit" name="submit" class="btn col-lg-1">Senden</button>
		</form>
		</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-3 save-container">

					<h2 class="font-bg">Gespeicherte Wörter</h2>

			</div>
			<div class="col-lg-9">
				<div class="term-Container background-yellow font-sm"></div>
			</div>
		</div>
	</div>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><script src="js/index.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


   <!-- Dokumente für Gamification: -->
     <?php include '../../php/gamification/gamification.php';?>
     <?php include '../../php/gamification/achievements.php';?>

	</body>
</html>
