<?php
session_start();
/*
include_once('../../php/system/data.php');
include_once('../../php/system/security.php');
*/

// Liebe Grüsse vom Backend-Team

     //create a new one-time-user:

   /* if (is_numeric($_GET['userIdentifier'])) { //make sure userIdentifier is a number

    $user_identifier = $_GET['userIdentifier']; //get userIdentifier from url via GET
}

   $userID = new_oneTimeUser($user_identifier); //create a new one-time-user in db and return id -> our userID

if(is_numeric($_GET['wsID'])) { //make sure wsID is a number
    $wsID = $_GET['wsID']; //get wsID from url via GET
}

    $ws_user_result = new_workshopforuser($wsID, $userID); //create connection between new one-time-user and workshop

   //depending on what you need:

   $_SESSION['userID'] = $userID; //save userID in a session-variable
   $_SESSION['wsID'] = $wsID; //save wsID in a session-variable
   */

?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">

  <!--
  EINSTIMMUNG:
  Nach 30 Sekunden auf der Seite, wird man automatisch auf die nächste Seite: insp_start.php weitergeleitet.

  mit content kann diese Zeit verändert werden
-->
  <meta http-equiv="refresh" content="30; URL=insp_start.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


  <title>focus.</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <style>
  body {background-color: black;}


/*
EINSTIMMUNG:
Folgender Code bettet das Video als HintergrundVideo ein
Der Text wird über dem Video eingeblendet.

 */
  #myVideo {
    position: fixed;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
  }

  .content {
    position: absolute;
    bottom: 50%;
    background: rgba(0, 0, 0, 0);
    color: #f1f1f1;
    width: 100%;
    text-align: center;
  }

  .subtitle {
    position: absolute;
    bottom: 10%;
    background: rgba(0, 0, 0, 0);
    color: #f1f1f1;
    width: 100%;
    text-align: center;
  }

  </style>
</head>
<body>



  <!-- The video -->
  <video autoplay muted playsinline id="myVideo">
    <source  src="../../php/gamification/gamification_vid.mp4" type="video/mp4">
    </video>

    <audio autoplay playsinline>
      <source src="../../php/gamification/gamification_audio.mp3" type="audio/mpeg">
      </audio>

      <!-- Optional: some overlay text to describe the video -->
      <h1 class="content">focus.</h1>
      <p class="subtitle">The brainstorming starts soon.</p>


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      <script src="../../js/main.js"></script>

      <!-- Dokumente für Gamification: -->
      <?php include '../../php/gamification/gamification.php';?>
      <?php include '../../php/gamification/achievements.php';?>

    </body>
    </html>
