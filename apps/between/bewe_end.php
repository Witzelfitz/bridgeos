<?php
    session_start();

    include_once('../../php/system/data.php');

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Inspiration Start</title>
</head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Allgemeines CSS--> 
	<link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="../../css/style_pinkslabs.css">

<body>
	
	
	<!-- Hintergrundbild-->
	<div class="bg">
		<!-- Fortschritt Balken 7 Squares + Timer  -->
	
		<div class="container">
		<div class="row">
		
		<div class="status">
			<div class="status__fortschritt">
				<div class="col-lg-1">
					<div class="square"></div>
				</div>
				<div class="col-lg-1">
					<div class="square"> </div>
				</div>
				<div class="col-lg-1">
					<div class="square"></div>
				</div>		
				<div class="col-lg-1">
					<div class="square"></div>
				</div>		
				<div class="col-lg-1">
					<div class="square"></div>
				</div>		
				<div class="col-lg-1">
					<div class="square"></div>
				</div>		
				<div class="col-lg-1">
					<div class="square"></div>
				</div>
			</div>
			<div class="status__timer">
				<div class="col-lg-2"></div>
				<div class="col-lg-3">Timer</div>
			</div>
		</div>
	</div>
</div>
	<!-- Lädt Gamification-Button und Timer-Zeile-->
  <div class="timer-buttons"></div>
		
	<!-- Intro -->
	<div class="container" style="margin-top: 20px;">

		
		<h1>Teil 3: Bewertung beendet</h1>
    <p>Vielen Dank für Ihre Teilnahme am Workshop. Sie haben alle Phasen
erfolgreich durchlaufen. Ihr Moderator wird Sie über das weitere
Vorgehen informieren.</p>


	</div>
	
			
	
  
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
<!-- Dokumente für Gamification: -->
  <?php include '../../php/gamification/gamification.php';?>
  <?php include '../../php/gamification/achievements.php';?>

        
</body>
</html>