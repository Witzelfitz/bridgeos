var list = this.parent;
var parentUl = "";
var taskCompleted = false;
var combiCounter = 0;

list.addEventListener('click', function(ev) {
	var li2Count = 0;
	var objectClass = ev.target.className;

	if(objectClass.indexOf("res") != -1 || objectClass.indexOf("commentText") != -1){
		//alert("No click");
	}else{
		if(!taskCompleted){
			//if(ev.target.className !== "res"){
			if (ev.target.tagName === 'LI') {
			  if(ev.target.className === "li1"){
				  parentUl = "myUL2";
			  }else{
				  if(ev.target.className === "li2"){
					parentUl = "myUL";
				  }
			  }

			  if(newElement(parentUl, ev.target.textContent)){
				  ev.target.remove();

			  }

			  //Ab hier nicht mehr nötig. Wurde aus reinen Sicherheitsgründen stehengelassen
			  li2Count = document.getElementsByClassName ("li2").length;
			}
		}
	}
}, false);

// Erstellt ein neues Listenobjekt (parentId = UL-Id; nodePar = Text; nodeId = Id Attribut von li Objekt)
function newElement(parentId, nodePar, nodeId) {
  var li = document.createElement("li");
  var inputValue = "Fehler"; //document.getElementById("myInput").value;
  var textPar = nodePar;
  var toggle = true;
  var parentUl = parentId;
  var listClass = "result";
  

  if(parentUl === "myUL2"){
	  listClass = "li2";
	  if(document.getElementsByClassName ("li2").length >=2){
		  toggle = false;
	  }
  }else{
	  if(parentUl === "myUL"){
	  	listClass = "li1";
	  }else{
	    parentUl = parentId;
		listClass = "res";
	  }
  }
	
  if(typeof nodeId != "undefined"){
  	listClass = nodeId;
	  
  }else{
	//alert("nodeId ist undefiniert");
  }
	
  if(toggle){
	  if(textPar !== undefined){
		  if(textPar.length > 0){
			inputValue = textPar;
		  }else{
			inputValue = "undefined1";
		  }
	  }
	  var t = document.createTextNode(inputValue);
	  
	  li.appendChild(t);
	  li.classList.add(listClass);
	  if (inputValue === '') {
		inputValue = "undefined2";
	  } else {
		document.getElementById(parentId).appendChild(li);
	  }
	  document.getElementById("myInput").value = "";

	}
	return toggle;
}

//Setzt 1 bis 2 Ideen mit einem Kommentar zusammen, erstellt HTML Objekte damit und sendet die Ideenkombination an die Datenbank
function overview(){
	var myNodelist = document.getElementsByClassName("li2");
	var value1 = "";
	var value2 = "";
	var sql = "";
	var result = "";
	

	if(myNodelist.length === 2){
		value2 = myNodelist[1].textContent;
	}
	
	if( myNodelist.length >= 1){
		value1 = myNodelist[0].textContent;
	}
	
	var value3 = document.getElementById("myInput").value;
	var strCounter = "";
	
	if(value3 === ""){
		alert("Kommentarfeld darf nicht leer sein!");
	}else{
		if(value1 === ""){
			alert("Bitte klicken Sie oben mindestens eine Inspiration an!");
		}else{
			combiCounter = combiCounter + 1;
			strCounter = combiCounter.toString();
			var log = document.createElement('ul');
			log.id = "result"+strCounter;
			log.class = "results vert";
			document.getElementById('resultbox').appendChild(log);

			if(value1){
				newElement("result"+strCounter, value1);
				newElement("myUL", value1);
			}

			if(value2){
				newElement("result"+strCounter, value2);
				newElement("myUL", value2);
			}

			newElement("result"+strCounter, value3, "commentText");

			document.getElementById('myUL2').innerHTML = "";
			
			send_sql_post("val1="+value1+"&val2="+value2+"&val3="+value3);
		}
	}
}

//Erstellt noch eine Dummy-Liste (Datenbankanbindung unklar)
createList();

function createList(){
	var ul = document.createElement('ul');
	ul.setAttribute('id','myUL');

	productList = ['Dummy1','Dummy2','Dummy3','Dummy4','Dummy5','Dummy6','Dummy7','Dummy8','Dummy9','Dummy10'];
	productList.sort(function() { return 0.5 - Math.random() });

	document.getElementById('inspiration').appendChild(ul);
	productList.forEach(renderProductList);

	function renderProductList(element, index, arr) {
		var li = document.createElement('li');
		li.setAttribute('class','li1');
		ul.appendChild(li);

		li.innerHTML=li.innerHTML + element;
	}
}

//Test für send_sql_post Funktion
//send_sql_post("val1=test1&val2=test2&val3=funktioniert");

//Ajax Funktion (sendet SQL Abfragen via GET an die PHP Datei 'send_sql.php')
function send_sql_post(strPost){
	$.ajax("./send_sql.php?"+strPost, {
		complete: function(response, status) {
			if (status == "success") {
				$("#txtHint").html(response.responseText);
			}
		}
	});
}