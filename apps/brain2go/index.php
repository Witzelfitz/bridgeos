<?php
session_start();

include_once('../../php/system/data.php');
//include_once('../../php/system/security.php');

//$_SESSION['questionID'] = $_GET['questionID'];

// Solange der Login-Link noch nicht funktioniert, muss mit Dummy-Werten getestet werden.
// Die nachfolgenden beiden Zeilen bitte löschen, sobald der Login-Link funktioniert (siehe dazu Zelle H12 im Excel "Anfragen von Gruppen" auf Google Drive)
$_SESSION['wsID'] = 1;
$_SESSION['userID'] = 2;
$_SESSION['questionID'] = 2;

//Session abfragen
$questionID = $_SESSION['questionID'];
$workshopID = $_SESSION['wsID'];
$userID = $_SESSION['userID'];

//Frage aus der Datenbank holen
$frage = get_question($questionID);
$frage = mysqli_fetch_assoc($frage);
$frageText = $frage['questionText'];
$zeit = $frage['questionDuration'];

?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">

  <meta name="viewport"
  content="width=device-width, initial-scale=1.0">

  <title>Brain2go</title>

  <!--Einbindung css-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="..\..\css\style.css">
  <link rel="stylesheet" type="text/css" href="css\style_brain2go.css">

</head>
<body>

  <!-- Lädt Gamification-Button und Timer-Zeile-->
  <div class="timer-buttons" id="bar-k"></div>

  <!--Hintergrundbild-->
  <div class="bg">

    <!-- Brain2go body-->
    <div class="container">
      <div class="row" >

        <div class="brain2go">
          <div class="col-lg-12">

            <!--Frage-->
            <div class ="bg-postit-right">
                <div class="brain2go__question">
                  <h1> <?php echo $frageText ?> </h1>
                </div>
              </div>

            <!--Titel der Seite-->
            <div class="col-lg-6" style="margin-top: 20px;">

              <!--Würfelbild und Hintergrundspostit-->
              <div class ="bg-postit-left" style="background-image: url(../../img/New-Brain2GoPost-it1.svg);">
                <div class="brain2go__image" class="img-responsive">
                  <img class="img-responsive" src="alle_icons_brain2go.jpg" alt="">
                </div>
                <div class="brain2go__button" style="margin-top: 20px;">
                  <button type="button" class="btn btn-info" id="button">Würfeln</button>
                </div>
              </div>
            </div>

              <!-- Eingabefeld der Antwort-->
              <form class="form todo-form" novalidate>
                <div class="input-group todo-input-group" class="brain2go__answerlist_bg">
                  <input type="text" class="form-control todo-input" placeholder="Antwort" tabindex="1">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                      <i class="glyphicon glyphicon-plus"></i>
                    </button>
                  </span>
                </div>
              </form>

              <!--Antwortfelder-->
              <div class="container container-list">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="todo-list list-group">
                      <!-- Hier kommen die Tasks hinein -->
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><script src="js/index.js"></script>

  <!-- Dokumente für Gamification: -->
  <?php include '../../php/gamification/gamification.php';?>
  <?php include '../../php/gamification/achievements.php';?>

</body>
</html>
