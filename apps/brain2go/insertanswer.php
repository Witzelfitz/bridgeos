<?php
session_start();

include_once('../../php/system/data.php');
include_once('../../php/system/security.php');

// Solange der Login-Link noch nicht funktioniert, muss mit Dummy-Werten getestet werden.
// Die nachfolgenden beiden Zeilen bitte löschen, sobald der Login-Link funktioniert (siehe dazu Zelle H12 im Excel "Anfragen von Gruppen" auf Google Drive)
$_SESSION['questionID'] = 2;
$_SESSION['wsID'] = 1;
$_SESSION['userID'] = 2;

$questionID = $_SESSION['questionID'];
$workshopID = $_SESSION['wsID'];
$userID = $_SESSION['userID'];

//new_answer function aus data.php, die die Antworten in die DB schreibt
new_answer($_GET["answer"], 0, 0, $workshopID, $questionID, $userID, 0);

?>
