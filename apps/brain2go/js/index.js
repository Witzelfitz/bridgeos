
/* Auflistung der Antworten */
var todos = [];

//erstellt die Auflistungen der Antworten im html mit dem eingeschriebenem Wert
function todo_html(todo) {
  return `
  <div class="todo-item list-group-item" class="brain2go__answerlist_bg">
  <input type="text" class="form-control todo-item-input" maxlength="25" placeholder="Todo..." tabindex="1" value="${ todo }">
  </div>`;
}

function add_todo(todo) { //fügt eine Antwort hinzu
  todos.push({
    todo: todo,
    done: false
  });

  var html = todo_html(todo); //fügt die Antwort untereinander hinzu / listet auf
  $('.todo-list').append(html);

}

function get_index(element) { //???
  return $(element).closest('.todo-item').index();
}

$('.todo-form').on('submit', function() { //wenn auf Submit gedrückt wird, wird die Antwort aufgelistet

  var todo = $('.todo-input').val();

  if (todo.length > 0) { //wenn nichts eingegeben wird, return false
    add_todo(todo);
    $('.todo-input').val('');

    //sobald auf Submit gedrückt wird, wird der Value vom Todo (was man als Antwort geschrieben hat) an die DB gesendet
    //mit dem $.get wird die Antwort an den Link geschickt, auf dieser Seite ist dann die function new_answer, die die Antwort in die DB schreibt
    $.get("http://618277-12.web1.fh-htwchur.ch/bridgeos/apps/brain2go/insertanswer.php?answer=" + todo,
    function (data){
    });

  };

  return false;
});

$('#button').on('click', function() { //wenn auf den Button gedrückt wird, wird ein random Bild ausgegeben
  //mit dem $.get wird die Antwort an den Link geschickt, auf dieser Seite ist dann die php-function, die random ein Bild anzeigt
  $.get("http://618277-12.web1.fh-htwchur.ch/bridgeos/apps/brain2go/wuerfeln.php",
  function (data){
    $(".brain2go__image").html(data);
  });
});

//Funktionen aus dem alten, kopierten Code. Die braucht's eigentlich nicht, wir behalten sie aber sicherheitshalber
/*function toggle_todo(element, index) { //
  todos[index].done = !todos[index].done;

  $(element).toggleClass('btn-success', todos[index].done);
}*/

/*$('.todo-list').on('input', '.todo-item-input', function() {
  var todo = $(this).val();
  var index = get_index(this);

  if (todo.length) {
    edit_todo(todo, index);
  } else {
    remove_todo(index);
  }

});*/
