<?php
    //includes functions from data.php
    include_once('../../php/system/data.php');

    //start Session and defin IDs (only for testing: will be given from the exercise before)
    session_start();
    $_SESSION['questionID'] = 2;
    $_SESSION['wsID'] = 1;
    $_SESSION['userID'] = 1;

    //save session variable into local variable
    $questionID = $_SESSION['questionID'];
    $wsID = $_SESSION['wsID'];
    $userID = $_SESSION['userID'];

    //create empty error_message variable
    $msg = "";

    //check if submit button was pressed
    if(isset($_POST['create_submit'])){
      //check if entry (from answer-inputfield) is not empty
      if(!empty($_POST['eingabe'])){
        //value will be saved in $answer
        $answer = $_POST['eingabe'];
        //delete all html and php tags
        $answer = strip_tags($answer);
        //checking again if its emtpy (after deleting tags)
        if(!empty($answer)){
            //content will be saved in database
            new_answer($answer, 0, 0, $wsID, $questionID, $userID);

          } else $msg = "Ungültige Eingabe"; //if the input contains only tags, this error-message will be saved
      } else $msg = "Bitte schreibe eine Antwort"; //if there is no text in the input, this error-message will be saved
    };
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <!--tab title-->
        <title>pinkLABs</title>
    </head>
      <!--integrates bootstrap-->
	   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <!-- Allgemeines CSS-->
	   <link rel="stylesheet" type="text/css" href="../../css/style.css">
     <link rel="stylesheet" type="text/css" href="../../css/style_pinkslabs.css">

     <body>
      <!--Background-->
         <div class="container col-lg-12">
    	   <div class="bg col-lg-10 col-lg-offset-1">
            <!-- Placeholder for Gamification-->
            <div class="timer-buttons" id="bar-k"></div>
            <!-- placeholder for introduction-text-->
              <div class="intro-pl"></div>
          </div>
          </div>

        <!--container with form and answers-->
        <div class="container col-lg-12 col-sm-12 col-12" style="margin-top: 20px;">

                        <!--form tag including button, textfield & question.-->
                        <form class="container col-lg-offset-3" method="post">
                            <!-- check if error-message is set and create error message (div) -->
                            <?php
                            if($msg!=""){
                             echo '<div class="row"><div class="alert alert-danger col-lg-6">'.$msg.'</div></div>';
                            }?>
                            <div class="row">
                            <!--Variable $questionID defines the question-->
                            <h2 class="font-bg"><?php get_question_text($questionID)?></h2>
                            <!-- input with maxlength 30 -->
                            <input class="answer col-lg-5" type="text" maxlength="25" name="eingabe" placeholder="Antwort" id="id_eingabe">
                            <!--if button is pressed, site will be reloaded and sent true (row 20)-->
                            <input class="btn pinklabs_btn-primary col-lg-1" name="create_submit" type="submit" value="Senden">
                            </div>
                        </form>


          <!--container for all answers-->
        	<div class="container answer-container">
                <div class="row">
                    <div class="answer-list text-center col-lg-offset-1 col-lg-10">
                        <!--get all answers (text) by questionID and userID-->
                        <?php get_answers_text($questionID,$userID) ?>
                    </div>
                </div>
        	</div>
        </div>

        <!--includes Javascript -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- documents for Gamification: -->
        <?php include '../../php/gamification/gamification.php';?>
        <?php include '../../php/gamification/achievements.php';?>

      </body>
</html>
