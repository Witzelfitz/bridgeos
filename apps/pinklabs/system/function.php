<?php
    include_once('../../php/system/data.php');

    // get questionText entry from question row by questionID
    function get_question_text($questionID){
    $result = get_question($questionID);
    $result = mysqli_fetch_assoc($result);
        echo $result['questionText'];
    }

    // get all answers (text) by questionID and user ID
    function get_answers_text($questionID, $userID){
    $result = get_answers($questionID, $userID);
    while($answer = mysqli_fetch_assoc($result)){
            echo '<p>'.$answer['answer'].'</p>';
        }
    }
    
?>