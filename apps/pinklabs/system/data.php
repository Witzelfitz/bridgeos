<?php
    include "config.php";

// Function for Connection to DB
    function get_db_connection()
	{
        $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (mysqli_connect_error()) {
            die('Verbindungsfehler (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        mysqli_query($db, "SET NAMES 'utf8'");
        return $db;
	}

// get Result from DB
    function get_result($sql)
	{
		$db = get_db_connection();
		$result = mysqli_query($db, $sql);
		mysqli_close($db);
		return $result;
	}

// get question row by questionID
    function get_question($questionID){
		$sql = "SELECT * FROM question WHERE questionID=$questionID;";
		return get_result($sql);
	}

// get questionText entry from question row by questionID
    function get_question_text($questionID){
    $result = get_question($questionID);
    $result = mysqli_fetch_assoc($result);
    echo $result['questionText'];
  }

// get answer by questionID and userID
    function get_answers($questionID, $userID){
		$sql = "SELECT * FROM answer WHERE questionID=$questionID AND userID=$userID;";
		return get_result($sql);
	}

// get all answers (text) by questionID and user ID
    function get_answers_text($questionID, $userID){
    $result = get_answers($questionID, $userID);
    while($answer = mysqli_fetch_assoc($result)){
      echo '<p>'.$answer['answer'].'</p>';
    }
  }

// write answers into DB
    function new_answer($answer, $parentAnswer, $parentAnswerTwo, $wsID, $questionID, $userID, $answerRelevance){
		$sql = "INSERT INTO answer (answer, parentAnswer, parentAnswerTwo, wsID, questionID, userID, answerRelevance) VALUES ($answer, $parentAnswer, $parentAnswerTwo, $wsID, $questionID, $userID, $answerRelevance);";
		return get_result($sql);
	}

?>
