
<!-- Hier die Links anpassen '../../php/system/data.php' und '../../php/system/security.php' -->
<?php include_once('php/system/data.php')?>
<?php include_once('php/system/security.php')?>

<!-- Hier ist der Platz für eigene PHP Befehle -->


<!doctype html>
<html lang="de">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <!-- BridgeOS CSS -->
    <link rel="stylesheet" href="http://618277-12.web1.fh-htwchur.ch/bridgeos/css/import.css" crossorigin="anonymous">

	<!--Titel anpassen auf Projektnamen z.B. Brain2Go -->
    <title>BridgeOS</title>
  </head>


  <body>
	  <header></header>
	  <nav></nav>
	  <!--ID anpassen auf Projektnamen z.B. Brain2Go -->
	  <main id="homepage"></main>




	  <footer></footer>
    <h1>Hello, world!</h1>

     <a href="apps/bewertung/">Bewertung</a> </br>
	 <a href="apps/brain2go/">brain2go</a> </br>
	 <a href="apps/kompression/">Kompression</a> </br>
	 <a href="apps/pinklabs/">pinklabs</a> </br>
	 <a href="apps/post2paper1/">post2paper1</a> </br>
	 <a href="apps/post2paper2/">post2paper2</a> </br>
	 <a href="apps/simplyprototyping/">simplyprototyping</a> </br>

<a href="apps/between/einstimmung.php" class="btn btn-default">Start</a>
    <!-- BridgeOS JavaScript -->
    <script src="http://618277-12.web1.fh-htwchur.ch/bridgeos/js/main.js" crossorigin="anonymous"></script>






    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>









</html>
