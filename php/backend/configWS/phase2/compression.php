﻿<?php
session_start();
if(isset($_SESSION['userid'])) {
    $logged_in = true;
    $user_id = $_SESSION['userid']; // get user-id from session-variable. 
} else { $logged_in = false; header('Location: ../../login.php'); //if user-id is not set, user is redirected to login.php
       }

//data.php is loaded:
require_once("../../../system/data.php");

$result = get_user($user_id);//getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname = $userInfos['userFirstname'];
$nachname = $userInfos['userLastname'];


if (is_numeric($_GET['id'])) {
    
    $ws_id = $_GET['id']; //get workshop-id from url via GET & store it locally as a variable.
    
}
$msg = "";

//validate form for creating a task:
if(isset($_POST['taskSubmit'])){
    $formRight = true;
    

if(!empty($_POST['question'])) {
$question = $_POST['question'];
} else {
  $msg .= "keine Frage eingegeben.<br>";
    $formRight = false;
    
}

    
    if(!empty($_POST['maxAnw'])) {
        $maxAnw = $_POST['maxAnw'];
        if(is_numeric($maxAnw)) { //additionally verify if answer for max. answers is a number & otherwise set corresponding error message.
            $numAnw = intval($maxAnw);
        } else {
            $msg .= "Bitte bei maximale Anzahl Antworten nur Zahlen eingeben.<br>";
            $formRight = false;
        }
        
    } else {
        $msg .= "Bitte geben Sie die maximale Anzahl Antworten ein.<br>";
        $formRight = false;
    }

if(!empty($_POST['time'])) {
    $time = intval($_POST['time']);
} else {
    $msg .= "Bitte Zeitangabe definieren.<br>";
    $formRight =false;
}
    
    if($formRight){
        
   //find out how many tasks have been created in phase 1 and what questionOrder is needed for compression:
 $result_num = get_amount_of_questions($ws_id);
   $num_t =  mysqli_fetch_assoc($result_num);
    $order_compression = ($num_t['questionOrder']) + 1; 
   

        // if user-inputs are valid, write them in db:
        $result = new_question($question, $time, $numAnw, $order_compression, 1, 6, $ws_id);
    if($result) {
            header('Location: ../WS.php?id=' . $ws_id);
        
    }else {
        $msg .= "Es gibt ein Problem mit der Datenbankverbindung<br>";
    }
    }
    
    }
   

    



?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../css/backend.css">
    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../css/task.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>Phase 2 - Kompression</title>
</head>

<body>
	        <p class="eingeloggt">Sie sind eingeloggt als
            <span style="color:blue"><?php
echo $vorname . " " . $nachname;
?></span>
        </p>
		<br/>
		<br/>
		<br/>
		<br/>
    <div class="container">
        <h4>Phase 2</h4>
        <h1>Kompression</h1>


<?php if(isset($ws_id)){ //making sure, there really is a workshop-id
        ?> 
        <form class="" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id;?>" method="post" enctype="multipart/form-data">
            
            <div class="form-group">
                <label for="question">Fragestellung</label>
                <input type="text" class="form-control" id="question" name="question">
            </div>
            <div class="form-group">
                <label for="time">Verfügbare Zeit:</label>
                <select class="form-control form-control-sm" id="time" name="time">
                    <option value="" selected>-</option>
                    <option id="30sec" value="30">30 Sekunden</option>
                    <option id="45sec" value="45">45 Sekunden</option>
                    <option id="1min" value="60">1 Minute</option>
                    <option id="2min" value="120">2 Minuten</option>
                    <option id="3min" value="180">3 Minuten</option>
                    <option id="4min" value="240">4 Minuten</option>
                    <option id="5min" value="300">5 Minuten</option>
                    <option id="6min" value="360">6 Minuten</option>
                    <option id="7min" value="420">7 Minuten</option>
                    <option id="8min" value="480">8 Minuten</option>
                    <option id="9min" value="540">9 Minuten</option>
                    <option id="10min" value="600">10 Minuten</option>
                </select>
            </div>
            <div class="form-group">
                <label for="maxAnw">Maximale Anzahl Antworten</label>
                <input class="form-control form-control-sm" name="maxAnw" id="maxAnw" type="text">
            </div>

           <a href="../WS.php?id=<?= $ws_id ?>" class="btn btn-primary" role="button" aria-pressed="true">Abbrechen</a>
            <button type="submit" name="taskSubmit" class="btn btn-primary">Speichern &amp; Zur Übersicht</button>
          

        </form>
        
            

        
             <?php } else {
    
    $msg .= "Workshop nicht gefunden. Workshop-ID fehlt.<br>";
} ?>
   
    <!-- if there are error messages, they are displayed here -->
    <?php if(!empty($msg)){ ?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php echo $msg ?>
        </p>
    </div>
    <?php } ?>

    </div>



</body>

</html>


