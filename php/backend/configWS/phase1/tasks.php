<?php
session_start();
if (isset($_SESSION['userid'])) {
    $logged_in = true;
    $user_id   = $_SESSION['userid']; // get user-id from session-variable. 
} else {
    $logged_in = false;
    header('Location: ../../login.php'); //if user-id is not set, user is redirected to login.php
}


//required external php-files are loaded:
include("backend_imgUpload.php");
require_once("../../../system/data.php");

$result    = get_user($user_id); //getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname   = $userInfos['userFirstname'];
$nachname  = $userInfos['userLastname'];

if (is_numeric($_GET['id'])) {
    
    $ws_id     = $_GET['id']; //get workshop-id from url via GET & store it locally as a variable.
    
}


if (is_numeric($_GET['order_id'])) {
    
    $order_id = $_GET['order_id']; //get task-id from url via GET
}

$msg = "";

//validate form for creating a task:
if ((isset($_POST['lastTask'])) || (isset($_POST['taskOn']))) {
    $formRight = true;
    
    if (!empty($_POST['taskType'])) {
        $type = intval($_POST['taskType']);
    } else {
        $msg .= "Bitte wählen Sie die Art der Übung aus.<br>";
        $formRight = false;
    }
    
    if (!empty($_POST['question'])) {
        $question = $_POST['question'];
    } else {
        $msg .= "keine Frage eingegeben.<br>";
        $formRight = false;
        
    }
    
    if (!empty($_POST['mandatory'])) {
        $mandatory = intval($_POST['mandatory']);
    } else {
        $mandatory = 0;
    }
    
    if (!empty($_POST['maxAnw'])) {
        $maxAnw = $_POST['maxAnw'];
        if (is_numeric($maxAnw)) {    //additionally verify if answer for max. answers is a number & otherwise set corresponding error message.
            $numAnw = intval($maxAnw);
        } else {
            $msg .= "Bitte bei maximale Anzahl Antworten nur Zahlen eingeben.<br>";
            $formRight = false;
        }
        
    } else {
        $msg .= "Bitte geben Sie die maximale Anzahl Antworten ein.<br>";
        $formRight = false;
    }
    
    
    
    
    if (!empty($_POST['time'])) {
        $time = intval($_POST['time']);
    } else {
        $msg .= "Bitte Zeitangabe definieren.<br>";
        $formRight = false;
    }
    
    
    if ($formRight) {
        // if user-inputs are valid, write them in db. questionOrder is set via $order_id:
        $question_id = new_question($question, $time, $numAnw, $order_id, $mandatory, $type, $ws_id);
        echo($question_id);
        
        if (isset($question_id)) {
            //if task-type is post2paper1 verify the img, if ok upload it and write url in db:
            if ($type == 2) {
                if(isset($_FILES['imageSelectField'])) {
                
                $imgGO = upload_post2paper1_IMG($question_id);
                
                if($imgGO) {
                echo($backend_img_array[3]);
                //write into db
                
                $imgDB = new_image($backend_root_folder.$backend_target_path, $backend_encodedFilename, $question_id);
                if($imgDB) {
                    echo("bild konnte in db geschrieben werden");
                } else {
                    $msg .= "Das Bild konnte nicht in der Datenbank gespeichert werden<br/>";
                }
                
                } else {
                    $msg .= $backend_img_array[3] . "<br/>";
                }
                }
            }
            //user is redirected depending on submit-button pressed:
            if (isset($_POST['lastTask'])) {
                 header('Location: phase1.php?id=' . $ws_id);
            } else {
                $order_id++;
                header('Location: tasks.php?id=' . $ws_id . "&order_id=" . $order_id);
            }
            
        } else {
            $msg .= "Es gibt ein Problem mit der Datenbankverbindung<br>";
        }
    }
}




?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../css/backend.css">
    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../css/task.css">
    <style>
        /* Input-Field for img-upload. default hidden. */

#postPaper_IMG {
    display: none;
}
    
    </style>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        //function for displaying/hiding input-field for img-upload depending on task-type selected.
        $(document).ready(function() {
    
             $("#taskType").change(function() {
                 if($(this).val() == 2){
                     console.log("in if bedingung drin");
                 $("#postPaper_IMG").show();
    } else {
        $("#postPaper_IMG").hide();
        console.log("in else bedingung drin");
    }
        });
            
            
    });
</script>
    
    <title>Übung <?php echo $order_id; ?></title>
</head>

<body>
	        <p class="eingeloggt">Sie sind eingeloggt als <span style="color:blue"><?php echo $vorname . " " . $nachname; ?></span></p>
		<br/>
		<br/>
		<br/>
		<br/>
    <div class="container">

        <h4>Phase 1</h4>
        <h1>Übung <?php echo $order_id; ?></h1>

        
<?php
if (isset($ws_id)) { //making sure, there really is a workshop-id
?>

        <form class="" action="<?php
    echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id . "&order_id=" . $order_id; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="taskType">Übungsart</label>
                <select class="form-control form-control-sm" id="taskType" name="taskType">
                    <option id="empty" value="" selected>-</option>
                    <option id="pinkLabs" value="1">p.i.n.k. labs</option>
                    <option id="post1_img" value="2">Post2Paper 1</option>
                    <option id="post2paper2" value="3">Post2Paper 2</option>
                    <option id="brain2go" value="4">Brain2Go</option>
                    <option id="prototype" value="5">Simply Prototyping</option>
                </select>
            </div>
            <div class="form-group">
                <label for="question">Fragestellung</label>
                <input type="text" class="form-control" id="question" name="question">
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="mandatory" name="mandatory" checked>
                <label class="form-check-label" for="mandatory">Übung obligatorisch</label>
            </div>
            <div class="form-group">
                <label for="time">Verfügbare Zeit:</label>
                <select class="form-control form-control-sm" id="time" name="time">
                    <option value="" selected>-</option>
                    <option id="30sec" value="30">30 Sekunden</option>
                    <option id="45sec" value="45">45 Sekunden</option>
                    <option id="1min" value="60">1 Minute</option>
                    <option id="2min" value="120">2 Minuten</option>
                    <option id="3min" value="180">3 Minuten</option>
                    <option id="4min" value="240">4 Minuten</option>
                    <option id="5min" value="300">5 Minuten</option>
                    <option id="6min" value="360">6 Minuten</option>
                    <option id="7min" value="420">7 Minuten</option>
                    <option id="8min" value="480">8 Minuten</option>
                    <option id="9min" value="540">9 Minuten</option>
                    <option id="10min" value="600">10 Minuten</option>
                </select>
            </div>
            <div class="form-group">
                <label for="maxAnw">Maximale Anzahl Antworten</label>
                <input class="form-control form-control-sm" name="maxAnw" id="maxAnw" type="text">
            </div>


            <div id="postPaper_IMG" class="custom-file">
                
                <input type="file" accept="image/*" id="imageSelectField" name="imageSelectField">
            </div>

<a href="phase1.php?id=<?= $ws_id ?>" class="btn btn-primary" role="button" aria-pressed="true">Abbrechen</a>
            <button type="submit" name="lastTask" class="btn btn-primary">Speichern &amp; Zurück</button>
             <button type="submit" name="taskOn" class="btn btn-primary">Speichern &amp; Weiter</button>

        </form>
        
          <?php
} else {
    
    $msg .= "Workshop nicht gefunden. Workshop-ID fehlt.<br>";
}
?>

   
    <!-- if there are error messages, they are displayed here -->
    <?php
if (!empty($msg)) {
?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php
    echo $msg;
?>
        </p>
    </div>
    <?php
}
?>
 </div>


</body>

</html>
