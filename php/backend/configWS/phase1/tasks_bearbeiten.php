﻿<?php
session_start();
if (isset($_SESSION['userid'])) {
    $logged_in = true;
    $user_id   = $_SESSION['userid']; // get user-id from session-variable. 
} else {
    $logged_in = false;
    header('Location: ../../login.php'); //if user-id is not set, user is redirected to login.php
}

//required external php-files are loaded:
include("backend_imgUpload.php");
require_once("../../../system/data.php");

$result    = get_user($user_id); //getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname   = $userInfos['userFirstname'];
$nachname  = $userInfos['userLastname'];

if (is_numeric($_GET['id'])) {
    
    $ws_id = $_GET['id']; //get workshop-id from url via GET & store it locally as a variable.
    
}


if (is_numeric($_GET['task_id'])) {
    
    $task_id = $_GET['task_id']; //get task-id from url via GET
}


$task_array = get_question($task_id); //get all infos to current task from db via task-id
$task_array = mysqli_fetch_assoc($task_array); //actually turn task_array into a readable array

$msg = "";

//validate form for creating a task:
if (isset($_POST['update'])) {
    $formRight = true;
    
    if (!empty($_POST['taskType'])) {
        $type = intval($_POST['taskType']);
    } else {
        $msg .= "Bitte wählen Sie die Art der Übung aus.<br>";
        $formRight = false;
    }
    
    if (!empty($_POST['question'])) {
        $question = $_POST['question'];
    } else {
        $msg .= "keine Frage eingegeben.<br>";
        $formRight = false;
        
    }
    
    if (!empty($_POST['mandatory'])) {
        $mandatory = intval($_POST['mandatory']);
    } else {
        $mandatory = 0;
    }
    
    if (!empty($_POST['maxAnw'])) {
        $maxAnw = $_POST['maxAnw'];
        if (is_numeric($maxAnw)) {    //additionally verify if answer for max. answers is a number & otherwise set corresponding error message.
            $numAnw = intval($maxAnw);
        } else {
            $msg .= "Bitte bei maximale Anzahl Antworten nur Zahlen eingeben.<br>";
            $formRight = false;
        }
        
    } else {
        $msg .= "Bitte geben Sie die maximale Anzahl Antworten ein.<br>";
        $formRight = false;
    }
    
    
    
    
    if (!empty($_POST['time'])) {
        $time = intval($_POST['time']);
    } else {
        $msg .= "Bitte Zeitangabe definieren.<br>";
        $formRight = false;
    }
    
    
    if ($formRight) {
        // if user-inputs are valid, update question table. task_id is given to identify the task to modify:
        $result = update_question($task_id, $question, $time, $numAnw, $task_array['questionOrder'], $mandatory, $type);
        
        if ($result) {
            
                 //if task-type is post2paper1 verify the img, if ok upload it and write url in db:
            if ($type == 2) {
                if(isset($_FILES['imageSelectField'])) {
                
                $imgGO = upload_post2paper1_IMG($task_id);
                
                if($imgGO) {
                echo($backend_img_array[3]);
                    
                //write img into db:
                $imgDB = update_image($backend_root_folder.$backend_target_path, $backend_encodedFilename, $task_id);
                    
                if($imgDB) {
                    echo("bild konnte in db geschrieben werden");
                } else {
                    $msg .= "Das Bild konnte nicht in der Datenbank gespeichert werden<br/>";
                }
                
                } else {
                    $msg .= $backend_img_array[3] . "<br/>";
                }
                
            }

            if (isset($_POST['update'])) {
                header('Location: phase1.php?id=' . $ws_id);
            } 
            
        } 
    } else {
            $msg .= "Es gibt ein Problem mit der Datenbankverbindung<br>";
        }
}
}



$img_result = get_image_by_questionID($task_id); //get all infos to img for question via task_id
$img_num =  mysqli_num_rows($img_result);   
if($img_num > 0) { //see if there actually is an img stored for this task
    echo($img_num);
    $img_array = mysqli_fetch_assoc($img_result); //actually turn img_array into a readable array
} else {
}



?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../css/backend.css">
    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../css/task.css">
    <style>
        /* Input-Field for img-upload. default hidden. */

#postPaper_IMG {
    display: none;
}
    
    </style>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        //function for displaying/hiding input-field for img-upload depending on task-type selected.
        $(document).ready(function() {
          var counter=0;
             $("#taskType").change(function() {
                 if($(this).val() == 2){
                     console.log("in if bedingung drin");
                 $("#postPaper_IMG").show();
                     if(counter<=0){
                 $( "#postPaper_IMG" ).append( "<br/><br/><br/><br/><br/>" );
                       counter++;  
                     }
    } else {
        $("#postPaper_IMG").hide();
        console.log("in else bedingung drin");
        $( "#postPaper_IMG" ).remove( "<br/><br/><br/><br/><br/>" );
    }
        });
            
            
    });
</script>
    
    <title>Übung 1</title>
</head>

<body>
	        <p class="eingeloggt">Sie sind eingeloggt als <span style="color:blue"><?php echo $vorname . " " . $nachname;?></span></p>
		<br/>
		<br/>
		<br/>
		<br/>
    <div class="container">

        <h4>Phase 1</h4>
        <h1>Übung 1</h1>

        
<?php
        //making sure, there really is a workshop-id
if (isset($ws_id)) {
?>

        <form class="" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id . "&task_id=" . $task_id; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="taskType">Übungsart</label>
                <select class="form-control form-control-sm" id="taskType" name="taskType">
                    <option id="pinkLabs" value="1"
                    <?php if ($task_array['typeID'] == 1){echo "selected";} ?>
                    >p.i.n.k. labs</option>
                    <option id="post1_img" value="2" 
                    <?php if ($task_array['typeID'] == 2){echo "selected";} ?>
                    >Post2Paper 1</option>
                    <option id="post2paper2" value="3" 
                    <?php if ($task_array['typeID'] == 3){echo "selected";} ?>
                    >Post2Paper 2</option>
                    <option id="brain2go" value="4" 
                    <?php if ($task_array['typeID'] == 4){echo "selected";} ?>
                    >Brain2Go</option>
                    <option id="prototype" value="5" 
                    <?php if ($task_array['typeID'] == 5){echo "selected";} ?>
                    >Simply Prototyping</option>
                </select>
            </div>
            <div class="form-group">
                <label for="question">Fragestellung</label>
                <input type="text" class="form-control" id="question" name="question" value="<?php echo $task_array['questionText'] ?>">
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="mandatory" name="mandatory" <?php if($task_array['questionSkippable'] == true){echo("checked");}?> >
                <label class="form-check-label" for="mandatory">Übung obligatorisch</label>
            </div>
            <div class="form-group">
                <label for="time">Verfügbare Zeit:</label>
                <select class="form-control form-control-sm" id="time" name="time">
                    <option id="30sec" value="30" 
                    <?php if ($task_array['questionDuration'] == 30){echo "selected";} ?>
                    >30 Sekunden</option>
                    <option id="45sec" value="45" 
                    <?php if ($task_array['questionDuration'] == 45){echo "selected";} ?>
                    >45 Sekunden</option>
                    <option id="1min" value="60" 
                    <?php if ($task_array['questionDuration'] == 60){echo "selected";} ?>
                    >1 Minute</option>
                    <option id="2min" value="120" 
                    <?php if ($task_array['questionDuration'] == 120){echo "selected";} ?>
                    >2 Minuten</option>
                    <option id="3min" value="180" 
                    <?php if ($task_array['questionDuration'] == 180){echo "selected";} ?>
                    >3 Minuten</option>
                    <option id="4min" value="240" 
                    <?php if ($task_array['questionDuration'] == 240){echo "selected";} ?>
                    >4 Minuten</option>
                    <option id="5min" value="300" 
                    <?php if ($task_array['questionDuration'] == 300){echo "selected";} ?>
                    >5 Minuten</option>
                    <option id="6min" value="360" 
                    <?php if ($task_array['questionDuration'] == 360){echo "selected";} ?>
                    >6 Minuten</option>
                    <option id="7min" value="420" 
                    <?php if ($task_array['questionDuration'] == 420){echo "selected";} ?>
                    >7 Minuten</option>
                    <option id="8min" value="480" 
                    <?php if ($task_array['questionDuration'] == 480){echo "selected";} ?>
                    >8 Minuten</option>
                    <option id="9min" value="540" 
                    <?php if ($task_array['questionDuration'] == 540){echo "selected";} ?>
                    >9 Minuten</option>
                    <option id="10min" value="600" 
                    <?php if ($task_array['questionDuration'] == 600){echo "selected";} ?>
                    >10 Minuten</option>
                </select>
            </div>
            <div class="form-group">
                <label for="maxAnw">Maximale Anzahl Antworten</label>
                <input class="form-control form-control-sm" name="maxAnw" id="maxAnw" type="text" value="<?php echo $task_array['questionMaxAnswer'] ?>">
            </div>


            <div id="postPaper_IMG" class="custom-file" style="height:auto">
                <?php if($img_num == 0){
    echo('<label for="imageSelectField">Aktuell kein Bild hochgeladen. Bild hochladen:</label> <br>');
    echo('<input type="file" accept="image/*" id="imageSelectField" name="imageSelectField">');
    echo('<br/><br/><br/>');
} else if($img_num > 0 || $task_array['typeID'] == 2){
    
    echo('<label for="imageSelectField">Aktuelles Bild:' . $img_array['imgAlttext'] . '. Bild ändern:</label> <br>');
    echo('<input type="file" accept="image/*" id="imageSelectField" name="imageSelectField">');
     echo('<br/><br/><br/>');
}
            ?>    
            </div>
            

<a href="phase1.php?id=<?= $ws_id ?>" class="btn btn-primary" role="button" aria-pressed="true">Abbrechen</a>
             <button type="submit" name="update" class="btn btn-primary">Speichern</button>

        </form>
        
          <?php
} else {
    
    $msg .= "Workshop nicht gefunden. Workshop-ID fehlt.<br>";
}
?>

    </div>
    <!-- if there are error messages, they are displayed here -->
    <?php
if (!empty($msg)) {
?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php
    echo $msg;
?>
        </p>
    </div>
    <?php
}
?>



</body>

</html>
