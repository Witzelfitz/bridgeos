<?php
session_start();
if (isset($_SESSION['userid'])) {
    $logged_in = true;
    $user_id   = $_SESSION['userid']; // get user-id from session-variable. 
} else {
    $logged_in = false;
    header('Location: ../../login.php'); //if user-id is not set, user is redirected to login.php
}

//data.php is loaded:
require_once("../../../system/data.php");

$result    = get_user($user_id); //getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname   = $userInfos['userFirstname'];
$nachname  = $userInfos['userLastname'];

if (is_numeric($_GET['id'])) {
    
    $ws_id = $_GET['id']; //get workshop-id from url via GET & store it locally as a variable.
    
    $task_list = get_questions_for_ws_phase1($ws_id); // execute function to get all tasks of a specific workshop by workshop-id and save it as a variable.
    
}
//Get info for current workshop for Message about Quantity of Tasks line 72
$ws_result = get_workshop_by_wsID($ws_id);
$ws_info = mysqli_fetch_assoc($ws_result);


?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="../../../../css/backend.css">
    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../css/task.css">


    <title>Phase 1 - Inspiration</title>
</head>

<body>
        <p class="eingeloggt">Sie sind eingeloggt als <span style="color:blue"><?php
echo $vorname . " " . $nachname;
?></span></p>
		<br/>
		<br/>
		<br/>
		<br/>
    <h1>Phase 1</h1>

<hr class="trennlinie">

    <main>

        <div class="container">
           <div class="wsList">
            <?php
if (isset($ws_id)) { //making sure, there really is a workshop-id
    $numT = mysqli_num_rows($task_list); //count the tasks created for this workshop
    if ($numT > 0) { //if there are tasks display amount for user in a Message and run through them with a while-loop
        ?>
        <p>Sie haben <?php echo $numT; ?> Übung<?php if($numT > 1) { echo("en"); } ?> für den Workshop <?php echo $ws_info['wsName']; ?> erstellt.</p>


        <div class="btn-group-vertical">
            <?php
        while ($task = mysqli_fetch_assoc($task_list)) { //while-loop
            $task_id = $task['questionID'];
?>


            <a href="tasks_bearbeiten.php?id=<?= $ws_id ?>&task_id=<?= $task_id ?>">
            <p><button class="btn btn-secondary"> <?php echo $task['questionText']; ?></button></p>
            </a>
            <br>


            <?php
        }
    } else {
?>
        <p>Momentan keine Übungen vorhanden.</p>
                <?php
    }
?>
     <div class="row">

           
                <div class="col">
                    <p><a href="tasks.php?id=<?= $ws_id ?>&order_id=<?= ($numT + 1) ?>"><button class="btn btn-primary">Übung erstellen</button></a></p>
                </div>
            </div>

</div>


</div>
        </div>


    </main>


    <div class="container">

        
        
<br/>
<br/>
<br/>
        
               
    <!-- optionale Nachricht (mit angepasstem CSS) -->
    <?php
    if (!empty($msg)) {
?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php
        echo $msg;
?>
        </p>
    </div>
    <?php
    }
?>
        
             <div class="row">
                <div class="col">
                    <a href="../WS.php?id=<?= $ws_id ?>"><button class="btn btn-primary">Zurück</button></a>
                </div>
                <div class="col">

                    <a href="../../index.php?id=<?= $ws_id ?>"><button class="btn btn-primary">Home</button></a>


                </div>
                <div class="col">

                    <a href="../../login.php"><button class="btn btn-primary">Log Out</button></a>


                </div>
            </div>
        

<hr class="trennlinie">



 



        <?php
} else {
    
    $msg .= "Workshop nicht gefunden. Workshop-ID fehlt.<br>";
}
?>
    
       <!-- optionale Nachricht (mit angepasstem CSS) -->
    <?php
if (!empty($msg)) {
?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php
    echo $msg;
?>
        </p>
    </div>
    <?php
}
?>
   </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
