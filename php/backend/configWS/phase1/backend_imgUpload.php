<?php
   

   
    //upload Image code - executed on sumbit
  function upload_post2paper1_IMG($question_id){
      global $backend_root_folder;
             $backend_root_folder = "php/backend/configWS/phase1/";
      global $backend_target_folder;
             $backend_target_folder = "images/";
             $backend_uploadOk = 1;
      global $backend_uploadMessage;
             $backend_uploadMessage = "";

        //create unique ID for each image -- encode filename to match server requirements -- create file path
        $backendID = uniqid();
 global $backend_encodedFilename;
        $backend_encodedFilename = urlencode(basename($_FILES["imageSelectField"]["name"]));

 global $backend_target_path;
        $backend_target_path = $backend_target_folder.$backendID.$backend_encodedFilename;


        //check if file is an image
        $check = getimagesize($_FILES["imageSelectField"]["tmp_name"]);

        if($check !== false) {
            $backend_uploadOk = 1;
        } else {
            $backend_uploadOk = 0;
            $backend_uploadMessage = "File is not an image";
        }


        // Check file size
        if ($_FILES["imageSelectField"]["size"] > 500000000) {
            $backend_uploadOk = 0;
            $backend_uploadMessage = "Sorry, your file is too large";
        }


    
        //upload image if there is no error
        if ($backend_uploadOk != 0) {

            // try to upload file
            if (move_uploaded_file($_FILES["imageSelectField"]["tmp_name"], $backend_target_path)) {

              

                //write uploadMessage
                $backend_uploadMessage = "Das Bild ". basename( $_FILES["imageSelectField"]["name"]). " wurde hochgeladen.<br>";
         global $backend_img_array;
                $backend_img_array = array($backend_root_folder, $backend_target_path, $backend_encodedFilename, $backend_uploadMessage);
                return $backend_img_array;

            } else {
                $backend_uploadMessage = "Es gab Schwierigkeiten beim Hochladen des Bildes<br>";
                 return $backend_img_array;
            } return $backend_img_array;
        } return $backend_img_array;
      
      
  }

?>
