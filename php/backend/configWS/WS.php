<?php
session_start();
if(isset($_SESSION['userid'])) {  // get user-id from session-variable.
    $logged_in = true;
    $user_id = $_SESSION['userid'];
} else { $logged_in = false; header('Location: ../login.php'); //if user-id is not set, user is redirected to login.php
       }
//data.php is loaded:
require_once("../../system/data.php");


$result = get_user($user_id);//getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname = $userInfos['userFirstname'];
$nachname = $userInfos['userLastname'];


if(is_numeric($_GET['id'])){

$ws_id = $_GET['id']; //get workshop-id from url via GET & store it locally as a variable.

}

//see in db if compression-task has already been defined.
$phase2_result = get_infos_for_compression($ws_id);
$phase2_exist = mysqli_num_rows($phase2_result);
$phase2_infos = mysqli_fetch_assoc($phase2_result);



$msg = "";




  /*
  * Create a random string
  * @author	XEWeb <>
  * @param $length the length of the string to create
  * @return $str the string
  * https://www.xeweb.net/2011/02/11/generate-a-random-string-a-z-0-9-in-php/
  */
  function randomString($length = 8) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
    	$rand = mt_rand(0, $max);
    	$str .= $characters[$rand];
    }
    return $str;
  }



if(isset($_POST['statusP1'])) {
   
    $live = status_live_phase1($ws_id);
    if($live) {
        //creating link with random hash & wsID
        $msg .= "Phase 1 freigeschaltet!<br>";
        $link = "http://618277-12.web1.fh-htwchur.ch/bridgeos/apps/between/einstimmung.php?userIdentifier=" . randomString() . "&wsID=" . $ws_id;
    } else {
        $msg .= "Phase 1 konnte nicht freigeschaltet werden.<br>";
    }
}

if(isset($_POST['statusP2'])) {
$live = status_live_phase2($ws_id);
    if($live) {
        //creating link with random hash & wsID
        $msg .= "Phase 2 freigeschaltet!<br>";
        $link = "http://618277-12.web1.fh-htwchur.ch/bridgeos/apps/kompression/index.php?userIdentifier=" . randomString() . "&wsID=" . $ws_id;
    } else {
        $msg .= "Phase 2 konnte nicht freigeschaltet werden.<br>";
    }
}

if(isset($_POST['statusP3'])) {
    $live = status_live_phase3($ws_id);
    if($live) {
        //creating link with random hash & wsID
        $msg .= "Phase 3 freigeschaltet!<br>";
        $link = "http://618277-12.web1.fh-htwchur.ch/bridgeos/apps/bewertung/index.php/?userIdentifier=" . randomString() . "&wsID=" . $ws_id;
    } else {
        $msg .= "Phase 3 konnte nicht freigeschaltet werden.<br>";
    }
}



?>

    <!doctype html>
    <html lang="en">
	
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../../../css/backend.css">
        <link rel="stylesheet" href="../../../css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../css/task.css">

        <title>Backend Home</title>
    </head>

    <body>
	         <p class="eingeloggt">Sie sind eingeloggt als
            <span style="color:blue"><?php
echo $vorname . " " . $nachname;
?></span></p>
		<br/>
		<br/>
		<br/>
		<br/>
        <h1>Backend</h1>
<hr class="trennlinie">

        <main>




        <div class="container">
		
	<div class="row">
         <?php if(isset($ws_id)){ ?>
    <div class="col-sm">
   <div class="btn-group-vertical">
       
      
       
		<p><a href="phase1/phase1.php?id=<?= $ws_id ?>"><button class="btn btn-primary">Phase 1</button></a></p>
		<br>
		<form class="" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id;?>" method="post">
            <p><button type="submit" name="statusP1" id="statusP1" class="btn btn-primary">Freischalten</button></p>
            </form>
		<br>
               
    <!-- optionale Nachricht (mit angepasstem CSS) -->
    <?php if(!empty($link)){ ?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php echo $link ?>
        </p>
    </div>
    <?php } ?>
       <br>
    </div>
	</div>
    <div class="col-sm">
      	<div class="btn-group-vertical">
      	<p><?php if($phase2_exist == 0) { echo('
		<a href="phase2/compression.php?id=' . $ws_id . '"><button class="btn btn-primary">Phase 2</button></a>');
                                     } else {
       echo('<a href="phase2/compression_bearbeiten.php?id=' . $ws_id . '&task_id=' . $phase2_infos['questionID'] . '"><button class="btn btn-primary">Phase 2</button></a>');
} ?></p>
		<br>
             <form class="" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id;?>" method="post">
            <p><button type="submit" name="statusP2" id="statusP2" class="btn btn-primary">Freischalten</button></p>
            </form>
		<br>
		</div>
    </div>
    <div class="col-sm">
		<div class="btn-group-vertical">
		<p><a href="phase3/auswertung.php?id=<?= $ws_id ?>"><button class="btn btn-primary">Phase 3</button></a></p>
		<br>
		<form class="" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $ws_id;?>" method="post">
            <p><button type="submit" name="statusP3" id="statusP3" class="btn btn-primary">Freischalten</button></p>
            </form>
		<br>

		
		</div>
    
	</div>
	           
        <?php } else {
    
    $msg .= "Workshop nicht gefunden. Workshop-ID fehlt.<br>";
} ?>
	</div>
		<br/>
		
 <div class="row">
                <div class="col">
                    <a href="../index.php?id=<?= $ws_id ?>"><button class="btn btn-primary">Home</button></a>
                

                    <a href="../login.php"><button class="btn btn-primary">Log Out</button></a>


                </div>
            </div>

		<hr class="trennlinie">
		
		</div>
            
                   </main>

        
   <!-- if there are error messages, they are displayed here -->
    <?php if(!empty($msg)){ ?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php echo $msg ?>
        </p>
    </div>
    <?php } ?>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>

    </html>
