<?php
session_start();
if(isset($_SESSION['userid'])) { // get user-id from session-variable.
    if(isset($_SESSION['ws_id'])){
        
    }
    $logged_in = true;
    $user_id = $_SESSION['userid']; 
} else { $logged_in = false; header('Location: ../../login.php'); //if user-id is not set, user is redirected to login.php
       }

//data.php is loaded:
require_once("../../../system/data.php");


$result = get_user ($user_id);//getting user-info from db.
// in order to adress the user personally, first- & last-name are saved as variables:
$userInfos = mysqli_fetch_assoc($result);
$vorname = $userInfos['userFirstname'];
$nachname = $userInfos['userLastname'];

if(is_numeric($_GET['id'])){

$ws_id = $_GET['id'];  //get workshop-id from url via GET & store it locally as a variable.


$idea_list = get_idea_from_wsID($ws_id); //get all info from all ideas created in this workshop

}


?>

    <!doctype html>
    <html lang="en">
	
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

       
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../../../css/backend.css">
    <link rel="stylesheet" href="../../../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../css/task.css">

        <title>Phase 3 - Auswertung</title>
    </head>

    <body>
		        <p class="eingeloggt">Sie sind eingeloggt als
            <span style="color:blue"><?php
echo $vorname . " " . $nachname;
?></span>
        </p>
		<br/>
		<br/>
		<br/>
		<br/>
        <h1>Auswertung</h1>



        <main>
		
<?php while($idea = mysqli_fetch_assoc($idea_list)){ //loop through array of ideas created in workshop and display a table for each
            $idea_id = $idea['ideaID']; //get ID for current idea
            ?>
			
<table class="table" >
  <thead>
    <tr>
      <th scope="col"><?php echo $idea['ideaTitle']; ?></th>
      <th scope="col">Anzahl User</th>
      <th scope="col">Bewertung</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      <td>
    <?php
    $numRate_result = amount_same_rating_same_idea(10, $idea_id); // get the amount of people who rated 10 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
    </td>
      <td>10</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>
           <?php
     $numRate_result = amount_same_rating_same_idea(9, $idea_id); // get the amount of people who rated 9 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
        </td>
      <td>9</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>
           <?php
    $numRate_result = amount_same_rating_same_idea(8, $idea_id); // get the amount of people who rated 8 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
        </td>
      <td>8</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(7, $idea_id); // get the amount of people who rated 7 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>7</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(6, $idea_id); // get the amount of people who rated 6 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>6</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(5, $idea_id); // get the amount of people who rated 5 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>5</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(4, $idea_id); // get the amount of people who rated 4 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>4</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(3, $idea_id); // get the amount of people who rated 3 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>3</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
    $numRate_result = amount_same_rating_same_idea(2, $idea_id); // get the amount of people who rated 2 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>2</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(1, $idea_id); // get the amount of people who rated 1 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>1</td>
    </tr>
          <tr>
      <th scope="row"></th>
      <td>
                 <?php
     $numRate_result = amount_same_rating_same_idea(0, $idea_id); // get the amount of people who rated 0 for current idea
    $numRate = mysqli_fetch_assoc($numRate_result);
    echo $numRate['count'];
    ?>
              </td>
      <td>0</td>
    </tr>
      
  </tbody>
</table>

<br/>
<?php } ?>
   




        </main>
	








		        <div class="container">
		
	<div class="row">
    <div class="col-sm">

    
	</div>
	            <div class="row">

                <div class="col">
                    <a href="../../index.php"><button class="btn btn-primary">Home</button></a>
                </div>
                <div class="col">

                    <a href="../../login.php"><button class="btn btn-primary">Log Out</button></a>


                </div>
            </div>
	</div>



		
		
		</div>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>

    </html>
