<?php
session_start();
if(isset($_SESSION['userid'])) unset($_SESSION['userid']); //if there is already a userid set, unset it. this is login.php
session_destroy(); // destroy existing session -> there will be a new one created


     $msg = "";
  $logged_in = true;
  //validate login form
  if(isset($_POST['logIn'])){
     if(!empty($_POST['mail'])){
    
      $mail = $_POST['mail']; 
         
    // Remove all illegal characters from email
     $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);

    // Validate e-mail
     if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    $msg .= "$mail ist keine gültige Email-Adresse";
     $logged_in = false;
    } 
    } else {
    $msg .= "Bitte geben Sie eine Email-Adresse ein.<br>";
      $logged_in = false;
    }
      
    if(empty($_POST['password'])){
      $msg .= "Bitte geben Sie Ihr Passwort ein.<br>";
      $logged_in = false;
    }else {
        $password = $_POST['password'];
    }   
      
      if($password && $mail) {
        include_once('../system/data.php');
        $result = login($mail, $password);
          if($result) {
              // amount of found entries saved in row_count $row_count
  		$row_count = mysqli_num_rows($result);
      if( $row_count == 1){
       
        session_start(); // here the new session is started
        $user = mysqli_fetch_assoc($result);
        $_SESSION['userid'] = $user['userID'];
        
        header('Location: index.php');
          exit;
      } else {
          echo("falsche Angaben");
      }
          } else {
              die("tschüss");
              $msg .= "login hat nicht funktioniert<br>";
              $logged_in = false;
          }
    } else {   
        echo("Leider falsch");
    }
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/backend.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">


    <title>Log in</title>
</head>

<body>
    <br>
    <br>

    <hr class="trennlinie">

    <h1 class="loginformular">Login</h1>
    <p id="einlogtext" class="loginformular">Bitte loggen Sie sich ein.</p>
    <br>
    <section class="msg">
        <form class="loginformular" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <div class="form-group">
                <label for="mail">Email-Adresse</label>
                <input type="email" name="mail" id="mail" class="form-control">
            </div>
            <br>
            <div class="form-group">
                <label for="password">Passwort</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>
            <button type="submit" name="logIn" class="btn btn-primary">Log in</button>
            <a href="register.php" class="btn btn-primary" role="button" aria-pressed="true">Registrieren</a>
        </form>
    </section>
    <hr class="trennlinie">

    <!-- if there are error messages, they are displayed here -->
    <?php if(!empty($msg)){ ?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php echo $msg ?>
        </p>
    </div>
    <?php } ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
