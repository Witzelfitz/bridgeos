<!doctype html>

<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../css/backend.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


    <title>Registrieren</title>




    <?php
  // validate register form
  $msg = "";
  $register_valid = true;
  
  if(isset($_POST['register_submit'])){

      
       if(!empty($_POST['firstname'])){
        $firstname = $_POST['firstname'];
    } else {
      $msg .= "Bitte geben Sie Ihren Vornamen ein.<br>";
      $register_valid = false;
    }  
      
       if(!empty($_POST['lastname'])){
       $lastname = $_POST['lastname'];
    } else {
     $msg .= "Bitte geben Sie Ihren Nachnamen ein.<br>";
      $register_valid = false;
    }
      
     if(!empty($_POST['mail'])){
    
      $mail = $_POST['mail']; 
         
    // Remove all illegal characters from email
     $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);

    // Validate e-mail
     if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    $msg .= "$mail ist keine gültige Email-Adresse";
     $register_valid = false;
    } 
    } else {
    $msg .= "Bitte geben Sie eine Email-Adresse ein.<br>";
      $register_valid = false;
    }
      
    if(!empty($_POST['password'])){
     $password = $_POST['password'];
    } else {
        $msg .= "Bitte geben Sie ein Passwort ein.<br>";
      $register_valid = false;
    }  
    
    if(empty($_POST['confirm_password'])){
          $msg .= "Bitte bestätigen Sie das Passwort.<br>";
      $register_valid = false;
    } 

    
    if(isset($password) && isset($confirm_password) && $password != $confirm_password){
      $msg .= "Passwort und Passwortbestätigung stimmen nicht überein.<br>";
      $register_valid = false;
    }
   
      if($register_valid){
    
      include_once('../system/data.php');
      $result = new_moderator($password, $mail, $firstname, $lastname);
          
      if(isset($result)){
       ?>
        <script>
            $(document).ready(function() {
                
                $('#myModal').on('shown.bs.modal', function() {
                    $('#myInput').trigger('focus');
                })
                $('#myModal').modal('show');
            });

        </script>
        <?php ;
      }else{
        $msg .= "Es gibt ein Problem mit der Datenbankverbindung.<br/>";
      }
      
  }
  }
  ?>
</head>


<body>
    <br>
    <br>
    <hr id="trennlinie" style=" width:400px; border:solid #fff 1px;height:1px;">
    <h1>Als Moderator registrieren</h1>
    <br>

    <section class="msg">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <div class="form-group">
                <label for="firstname">Vorname</label>
                <input type="text" name="firstname" id="firstname" class="form-control">
            </div>
            <br>
            <div class="form-group">
                <label for="lastname">Nachname</label>
                <input type="text" name="lastname" id="lastname" class="form-control">
            </div>
            <br>
            <div class="form-group">
                <label for="mail">Email-Adresse</label>
                <input type="email" name="mail" id="mail" class="form-control">
            </div>
            <br>
            <div class="form-group">
                <label for="id_password">Passwort: </label>
                <input type="password" name="password" id="id_password" class="form-control">
            </div>
            <div class="form-group">
                <label for="id_confirm_password">Passwort bestätigen: </label>
                <input type="password" name="confirm_password" id="id_confirm_password" class="form-control">
            </div>
            <input type="submit" name="register_submit" class="btn btn-default" value="registrieren">
        </form>
    </section>
    <hr id="trennlinie" style=" width:400px; border:solid #fff 1px;height:1px;">
    
     <!-- if there are error messages, they are displayed here -->
    <?php if(!empty($msg)){ ?>
    <div class="alert alert-info msg" role="alert">
        <p>
            <?php echo $msg; ?>
        </p>
    </div>
    <?php } ?>

    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sie haben sich erfolgreich registriert!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Bitte loggen Sie sich ein.</p>
                </div>
                <div class="modal-footer">
                    <a href="login.php"><button type="button" class="btn btn-primary">Zum Login</button></a>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
