<?php
session_start();
if (isset($_SESSION['userid'])) {
    $logged_in = true;
    $user_id   = $_SESSION['userid'];
} else {
    $logged_in = false;
    header('Location: login.php');
}


require_once("../system/data.php");
$result    = get_user($user_id);
$userInfos = mysqli_fetch_assoc($result);
$vorname   = $userInfos['userFirstname'];
$nachname  = $userInfos['userLastname'];


$ws_list = get_workshop_by_userID($user_id);

$msg       = "";
$logged_in = true;


if (isset($_POST['newWS'])) {
    
    if (!empty($_POST['title'])) {
        
        $WStitle = $_POST['title'];
        
    } else {
        $msg .= "Bitte geben Sie einen Titel für Ihren Workshop ein<br>";
    }
    
    if (!empty($_POST['aufgabenstellung'])) {
        $question = $_POST['aufgabenstellung'];
    } else {
        $msg .= "Bitte geben Sie die Aufgabenstellung an<br>";
    }
    
    if ($WStitle && $question) {
        include_once('../system/data.php');
        $ws_id = create_workshop($WStitle, $question, "blabla", 1, 2, 3, $user_id);
        if ($ws_id) {
            
            header('Location: configWS/WS.php?id=' . $ws_id);
            exit;
        } else {
            $msg .= "Es gibt Probleme mit der Datenbankverbindung.<br>";
        }
    }
}


?>

    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../../css/backend.css">
        <link rel="stylesheet" href="../../css/bootstrap.min.css">
        <link rel="stylesheet" href="../../css/task.css">

        <title>Backend Home</title>
    </head>

    <body>
        
		
        <p class="eingeloggt">Sie sind eingeloggt als <span style="color:blue"><?php echo $vorname . " " . $nachname; ?></span></p>
		<br/>
		<br/>
		<br/>
		<br/>

<h1>Backend</h1>
		<hr class="trennlinie">
        <main>
            <div class="container">
                
                <div class="wsList">
                    <?php
$num_w = mysqli_num_rows($ws_list);
if ($num_w > 0) {
?>
                   <p>Sie haben <?php echo $num_w; ?> Workshop<?php if($num_w > 1) { echo("s"); } ?> erstellt.</p>
                   
                   
                
                    <?php
    
    while ($ws = mysqli_fetch_assoc($ws_list)) {
?>
                        <a href="http://618277-12.web1.fh-htwchur.ch/bridgeos/php/backend/configWS/WS.php?id=<?= $ws['wsID']; ?>" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">
                            <?php
        echo $ws['wsName'];
?>
                        </a>
                        <br/><br/>
                        <?php
    }
} else {
?>

                    <p>Momentan keine Workshops vorhanden.</p>

                    
<?php
}
?>
                   
                    <div class="row">
                        <div class="col">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Neuen Workshop erstellen
                    </button>
                        </div>
                        <div class="col">

                            <a href="login.php"><button class="btn btn-primary">Log Out</button></a>


                        </div>
						
                    </div>
            </div>
			<hr class="trennlinie">
            </div>
        </main>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Neuer Workshop erstellen</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form class="" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                            <div class="form-group">
                                <label for="title">Titel Workshop</label>
                                <input type="text" name="title" class="form-control" id="title">
                            </div>
                            <div class="col"></div>
                            <div class="form-group">
                                <label for="aufgabenstellung">Aufgabenstellung</label>
                                <input type="text" name="aufgabenstellung" class="form-control" id="aufgabenstellung">
                            </div>
                            <button type="submit" name="newWS" class="btn btn-primary">Submit</button>
                        </form>
                         <!-- if there are error messages, they are displayed here -->
                        <?php
if (!empty($msg)) {
?>
                        <div class="alert alert-info msg" role="alert">
                            <p>
                                <?php
    echo $msg;
?>
                            </p>
                        </div>
                        <?php
}
?>
                    </div>
                </div>
            </div>
        </div>










        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>

    </html>