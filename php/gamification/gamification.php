

<?php include 'timer.php';?>

<script>

var timeout = `TimeOut`;

var myArray = [
  "Giraffe",
  "Elektroschock",
  "Chemie",
  "Schokolade",
  "Computer",
  "Spiel",
  "Schloss",
  "Landschaft",
  "Konzert",
  "König",
  "Flüchtling",
  "Poesie",
  "Erotik",
  "Mond",
  "Meer",
  "Bau",
  "Hunger",
  "Dame",
  "Prinzessin",
  "Superheld",
  "Achterbahn",
  "Bikini",
  "Geld",
  "Radio",
  "Musik",
  "Holz",
];

/* Randomize Funktion:
      Wenn auf Button "Zufallswort" gedrückt wird, wird das Module mit dem Zufallswort angezeigt:
      Dabei wird folgendes Wort angezeigt.
      Aus einem Array von Wörtern für die Inpsiration wird eines per Zufall ausgewählt und angezeigt:
*/

var randomItem = myArray[Math.floor(Math.random()*myArray.length)];

var page  = document.getElementById("full_name");

var nextpage = "../../index.php";

/* GAMIFICATION-BAR
      Die folgenden Variabeln werden jeweils in die einzelnen Übungen eingebaut.
      Daraus entsteht die Gamification-BAR, welche oben an der Seite angezeigt wird.

      Um die Gamifiaction-BAR je nach Seite variieren zu können,
      wurde dieser Code aufgeteilt.
*/
var bar_start = `<div class="alert alert-info" style="margin:auto" role="alert">
    <div class="container">
      <div class="row">
      `;

var bar_random = `
      <div class="col-sm-2">
        <!-- Button trigger modal -->
        <button" type="button" class="btn btn-info" id="randomword" " data-toggle="modal" data-target="#randomModal">
          Zufallswort</button>
        </div>`;

var bar_timeout = `<div class="col-sm-2">
  <input type="button" class="btn btn-warning" value=`+timeout+` id="time-pause" onclick="change(this)" name="3"></input>
    </div>`;

var bar_timeextension = `  <div class="col-sm-2">
    <button type="button" class="btn btn-warning" onclick="change2(this)" id="timeextension">Zeitverlängerung</button>
  </div>`;

var bar_timer =`  <div class="col-sm-2">
    <button type="button" aria-pressed="true" style="width:90px" disabled class="btn btn-outline"><span style="color:black;text-align:center;font-weight:bold"><div id="time-count-down" time="100" redirect-url=`+nextpage+`></div></span></button>
  </div>`;

var bar_nexpage = `  <div class="col-sm-2">
    <a href=`+nextpage+`><button type="button" id="time-pause-2" inactive class="btn  btn-success" id="time-end">Beenden & weiter</button></a>
  </div>`

var bar_end = `
        </div>
      </div>
    </div>
  </div>`;

  var bar_random2 = `  <!-- Modal -->
    <div class="modal fade" id="randomModal" tabindex="-1" role="dialog" aria-labelledby="randomModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="randomModalLabel">Keine Ideen mehr?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Das folgende Zufallswort bringt dich hoffentlich auf neue Gedanken:
            <h1>`+randomItem+`</h1>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Danke</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->`;


/*  Je nach Übunge Seite wird hier bestimmt, welche Elemente in der Gamification-BAR angezeigt werden.
    Die Append-Funktion hängt die jeweiligen Elemente im index.php der jeweiligen Übung an.
    Einzelne Buttons oder der Timer können einfach herausgelöscht oder ergänzt werden, wenn die
    entsprechenden Variabeln gelöscht/eingefügt werden.
    */
$('#bar-b2g').append(bar_start+bar_random+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end+bar_random2);

$('#bar-k').append(bar_start+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end);

$('#bar-p2p1').append(bar_start+bar_random+bar_random2+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end);

$('#bar-p2p2').append(bar_start+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end);

$('#bar-sp').append(bar_start+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end);

$('#bar-b').append(bar_start+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end);

$('#bar-pl').append(bar_start+bar_random+bar_timeout+bar_timeextension+bar_timer+bar_nexpage+bar_end+bar_random2);


var introduction_header = `<div class="modal" id="introModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">`

var introduction_footer =`<div class="modal-footer">
        <button type="button" id="time-pause-1" class="btn btn-primary" data-dismiss="modal">Übung starten</button>
      </div>
    </div>
  </div>
</div>`

  $('.intro-b2g').append(introduction_header+`
        <h5 class="modal-title">Brain2Go</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">Bei der nächsten Übung «Brain2Go» geht es darum, schnell und spontan zu sein. Mit einem Klick auf den Würfel-Button erscheint ein Symbol. Schreibe nun alle Stichworte auf, die dir zu diesem Symbol in Zusammenhang mit der Frage einfallen. Mit Enter bestätigst du die Eingabe. Wenn dir nichts mehr einfällt, klicke wieder auf den Würfel-Button, ein neues Symbol erscheint und du kannst neue Antworten schreiben. Wiederhole den Vorgang solange, bis die Übung beendet ist. </p>
      </div>`+introduction_footer);

  $('.intro-sp').append(introduction_header+`
        <h5 class="modal-title">Simply Prototyping</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">Auf der folgenden Seite hast du die Möglichkeit, eigene Bilder, Skizzen und Designideen im Bildformat hochzuladen. Ausserdem kannst du alle bereits hochgeladenen Bilder einsehen. </p>
      </div>`+introduction_footer);

  $('.intro-pl').append(introduction_header+`
        <h5 class="modal-title">Pink Labs</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">Auf der nächsten Seite finden Sie eine Frage zu einem beliebigen Thema. Versuchen Sie spontan und instinktiv in der vorgegebenen Zeit stichwortartig zu antworten.</p>
      </div>`+introduction_footer);

  $('.intro-p2p1').append(introduction_header+`
        <h5 class="modal-title">POST2PAPER 1</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">In der folgenden Aufgabe wird dir ein zufälliges Bild einer Person gezeigt. Überlege dir, was diese Person in ihrer Freizeit machen könnte. Schreibe deine Ideen in das Eingabefeld und drücke auf «Submit».</p>
      </div>`+introduction_footer);

  $('.intro-p2p2').append(introduction_header+`
        <h5 class="modal-title">POST2PAPER 2</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">In der nächsten Übung werden dir die Begriffe der  vorherigen Übung gezeigt. Wähle davon zwei aus und schreibe dazu eine Idee aus.</p>
      </div>`+introduction_footer);

  $('.intro-k').append(introduction_header+`
        <h5 class="modal-title">KOMPRESSION</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">Du befindest dich jetzt in der Ideenfindung. Du siehst alle Antworten von allen Teilnehmern. Lass dich durch die Wortwolke inspirieren. Du kannst einzelne Wörter per Klick zwischenspeichern. Durch diese Wörter solltest du in der Lage sein eine gute Idee zu entwickeln. Schreibe diese mit einem Titel und einer kurzen Beschreibung in das Eingabefeld.</p>
      </div>`+introduction_footer);

  $('.intro-b').append(introduction_header+`
        <h5 class="modal-title">BEWERTUNG</h5>
      </div>
      <div class="modal-body" style="text-align:justify; padding:8%";>
        <p class="lead">Bei der folgenden Übung geht es um die Bewertung von bisher gesammelten Ideen. Dabei kannst du den Ideen Bewertungen von 1 bis 10 geben. Schlussendlich werden die Ideen mit den meisten Punkten weiterdiskutiert.</p>
      </div>
      <div class="modal-footer">`+introduction_footer);


  window.onload = function () {
    $(document).ready(function () {
        $('#introModal').modal('show');
    })

  };


/* FUNKTION zum BUTTON TimeOut:
        Wenn der Button geklickt wird,
        ändert sich der Button-Text zu "Fortfahren"
        Sobald der Button wieder geklickt wird,
        ändert sich der Text zurück zu "TimeOut"*/
  function change(){
    if(document.getElementById("time-pause").value=="TimeOut"){
  document.getElementById("time-pause").value="Fortfahren";
}else{document.getElementById("time-pause").value="TimeOut";
}
}
  function change2(){
  document.getElementById("time-pause").value="TimeOut";
}

</script>
<script src="../../js/timer.js"></script>
