<?php

define('BRIDGE_INDEX_URL', "/bridgeos");
define('BRIDGE_APPS_URL', BRIDGE_INDEX_URL . "/apps");

/**
*   $workshop_id                Id of the current workshop
*   $completed_question_id      Id of the completed question
*   returns string              url of next question
*/
function getNextQuestionUrl($workshop_id, $completed_question_id){
    include_once('data.php');
    $result = getNextQuestion($workshop_id, $completed_question_id);
    if($result){
        return BRIDGE_APPS_URL . '/' . $result['url'];
    }else{
        return BRIDGE_INDEX_URL;
    }
}
?>
