# bridgeOS
Das Projekt wurde um 11:50 für alle Gruppen auf lesen gesetzt. Keine Uploads mehr möglich.

## Live Server
http://618277-12.web1.fh-htwchur.ch/bridgeos/

## Bitbucket
https://bitbucket.org/Witzelfitz/bridgeos/src/master/

## Allgemeine Funktionen
In diesem Kapitel sind alle Funktionen beschrieben, die von allen genutzt werden dürfen.

### Timer
Der Timer wird folgendermassen in die Seite eingebunden:
```html
<div id="time-count-down" time="50" redirect-url="http://www.link.ch/"></div>
```
- Das Attribut `time` enthält die Zeit in Sekunden an.
- Das Attribut `redirect-url` enthält den absoulten Link zur nächsten Seite.
Info: Das Element muss nicht unbedingt ein `div` sein, es darf auch mehr Attribute enthalten wie z.B. `class`.

### Gamification
